package util;

import java.io.InputStream;

/**
 * Created by Karthick on 17/9/18.
 */
public class CommonUtil {

    public static InputStream loadPropertyFile(ClassLoader classLoader) {
        InputStream inputStream = null;
        try {
            String environment = System.getProperty("environment");
            inputStream = classLoader.getResourceAsStream("config_" + environment + ".properties");
        }catch (Exception e) {
            e.printStackTrace();
        }
        return inputStream;
    }
}
