
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConditionsPrecedentEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ConditionsPrecedentEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Yes"/>
 *     &lt;enumeration value="No"/>
 *     &lt;enumeration value="Waived"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConditionsPrecedentEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum ConditionsPrecedentEnum {


    /**
     * Conditions precedent have been met.
     * 
     */
    @XmlEnumValue("Yes")
    YES("Yes"),

    /**
     * Conditions precedent have not been met.
     * 
     */
    @XmlEnumValue("No")
    NO("No"),

    /**
     * The requirement for conditions precedent were waived.
     * 
     */
    @XmlEnumValue("Waived")
    WAIVED("Waived");
    private final String value;

    ConditionsPrecedentEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConditionsPrecedentEnum fromValue(String v) {
        for (ConditionsPrecedentEnum c: ConditionsPrecedentEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
