
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LcTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LcTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Standby"/>
 *     &lt;enumeration value="Commercial"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LcTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum LcTypeEnum {

    @XmlEnumValue("Standby")
    STANDBY("Standby"),
    @XmlEnumValue("Commercial")
    COMMERCIAL("Commercial");
    private final String value;

    LcTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LcTypeEnum fromValue(String v) {
        for (LcTypeEnum c: LcTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
