
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BreakageCostEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BreakageCostEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="AgentBank"/>
 *     &lt;enumeration value="Lender"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BreakageCostEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum BreakageCostEnum {


    /**
     * Breakage cost is calculated by the agent bank.
     * 
     */
    @XmlEnumValue("AgentBank")
    AGENT_BANK("AgentBank"),

    /**
     * Breakage cost is calculated by the lender.
     * 
     */
    @XmlEnumValue("Lender")
    LENDER("Lender");
    private final String value;

    BreakageCostEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BreakageCostEnum fromValue(String v) {
        for (BreakageCostEnum c: BreakageCostEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
