
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DifferenceTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DifferenceTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Value"/>
 *     &lt;enumeration value="Reference"/>
 *     &lt;enumeration value="Structure"/>
 *     &lt;enumeration value="Scheme"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DifferenceTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum DifferenceTypeEnum {

    @XmlEnumValue("Value")
    VALUE("Value"),
    @XmlEnumValue("Reference")
    REFERENCE("Reference"),
    @XmlEnumValue("Structure")
    STRUCTURE("Structure"),
    @XmlEnumValue("Scheme")
    SCHEME("Scheme");
    private final String value;

    DifferenceTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DifferenceTypeEnum fromValue(String v) {
        for (DifferenceTypeEnum c: DifferenceTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
