
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WeeklyRollConventionEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WeeklyRollConventionEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="MON"/>
 *     &lt;enumeration value="TUE"/>
 *     &lt;enumeration value="WED"/>
 *     &lt;enumeration value="THU"/>
 *     &lt;enumeration value="FRI"/>
 *     &lt;enumeration value="SAT"/>
 *     &lt;enumeration value="SUN"/>
 *     &lt;enumeration value="TBILL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WeeklyRollConventionEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum WeeklyRollConventionEnum {


    /**
     * Monday
     * 
     */
    MON,

    /**
     * Tuesday
     * 
     */
    TUE,

    /**
     * Wednesday
     * 
     */
    WED,

    /**
     * Thursday
     * 
     */
    THU,

    /**
     * Friday
     * 
     */
    FRI,

    /**
     * Saturday
     * 
     */
    SAT,

    /**
     * Sunday
     * 
     */
    SUN,

    /**
     *  13-week and 26-week U.S. Treasury Bill Auction Dates. Each Monday except for U.S. (New York) holidays when it will occur on a Tuesday.
     * 
     */
    TBILL;

    public String value() {
        return name();
    }

    public static WeeklyRollConventionEnum fromValue(String v) {
        return valueOf(v);
    }

}
