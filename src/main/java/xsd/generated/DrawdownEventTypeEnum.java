
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DrawdownEventTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DrawdownEventTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="NewDrawdownEvent"/>
 *     &lt;enumeration value="RateSetEvent"/>
 *     &lt;enumeration value="FxRateSetEvent"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DrawdownEventTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum DrawdownEventTypeEnum {


    /**
     * Event triggered by the borrower requesting a new loan contract against an existing facility with the agent bank. The agent will receive the borrower notice, calculate the amount of principal due from each lender based on their respective share of the underlying commitment and send loan contract notices to the lenders.
     * 
     */
    @XmlEnumValue("NewDrawdownEvent")
    NEW_DRAWDOWN_EVENT("NewDrawdownEvent"),

    /**
     * This is a notice that defines when the actual underlying base rate is set for the loan contract period.
     * 
     */
    @XmlEnumValue("RateSetEvent")
    RATE_SET_EVENT("RateSetEvent"),

    /**
     * This is a notice that defines when the exchange rate is set for the loan contract period. Applicable only for multicurrency loan contracts.
     * 
     */
    @XmlEnumValue("FxRateSetEvent")
    FX_RATE_SET_EVENT("FxRateSetEvent");
    private final String value;

    DrawdownEventTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DrawdownEventTypeEnum fromValue(String v) {
        for (DrawdownEventTypeEnum c: DrawdownEventTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
