
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LcPurposeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LcPurposeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Financial"/>
 *     &lt;enumeration value="Performance"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LcPurposeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum LcPurposeEnum {

    @XmlEnumValue("Financial")
    FINANCIAL("Financial"),
    @XmlEnumValue("Performance")
    PERFORMANCE("Performance");
    private final String value;

    LcPurposeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LcPurposeEnum fromValue(String v) {
        for (LcPurposeEnum c: LcPurposeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
