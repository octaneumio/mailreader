
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketDisruptionEventsEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MarketDisruptionEventsEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Applicable"/>
 *     &lt;enumeration value="NotApplicable"/>
 *     &lt;enumeration value="AsSpecifiedInMasterAgreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MarketDisruptionEventsEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum MarketDisruptionEventsEnum {


    /**
     * Market Disruption Events are applicable.
     * 
     */
    @XmlEnumValue("Applicable")
    APPLICABLE("Applicable"),

    /**
     * Market Disruption Events are not applicable.
     * 
     */
    @XmlEnumValue("NotApplicable")
    NOT_APPLICABLE("NotApplicable"),

    /**
     * The Market Disruption Event(s) are determined by reference to the relevant master agreement.
     * 
     */
    @XmlEnumValue("AsSpecifiedInMasterAgreement")
    AS_SPECIFIED_IN_MASTER_AGREEMENT("AsSpecifiedInMasterAgreement");
    private final String value;

    MarketDisruptionEventsEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MarketDisruptionEventsEnum fromValue(String v) {
        for (MarketDisruptionEventsEnum c: MarketDisruptionEventsEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
