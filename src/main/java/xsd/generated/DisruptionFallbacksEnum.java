
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisruptionFallbacksEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DisruptionFallbacksEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="AsSpecifiedInMasterAgreement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DisruptionFallbacksEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum DisruptionFallbacksEnum {

    @XmlEnumValue("AsSpecifiedInMasterAgreement")
    AS_SPECIFIED_IN_MASTER_AGREEMENT("AsSpecifiedInMasterAgreement");
    private final String value;

    DisruptionFallbacksEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DisruptionFallbacksEnum fromValue(String v) {
        for (DisruptionFallbacksEnum c: DisruptionFallbacksEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
