
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeOfIndexSwapEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeOfIndexSwapEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Accrual"/>
 *     &lt;enumeration value="FairValue"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeOfIndexSwapEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum TypeOfIndexSwapEnum {


    /**
     * Accrual index swap type.
     * 
     */
    @XmlEnumValue("Accrual")
    ACCRUAL("Accrual"),

    /**
     * Fair Value index swap type.
     * 
     */
    @XmlEnumValue("FairValue")
    FAIR_VALUE("FairValue");
    private final String value;

    TypeOfIndexSwapEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeOfIndexSwapEnum fromValue(String v) {
        for (TypeOfIndexSwapEnum c: TypeOfIndexSwapEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
