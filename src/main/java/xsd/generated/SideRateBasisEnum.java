
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SideRateBasisEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SideRateBasisEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Currency1PerBaseCurrency"/>
 *     &lt;enumeration value="BaseCurrencyPerCurrency1"/>
 *     &lt;enumeration value="Currency2PerBaseCurrency"/>
 *     &lt;enumeration value="BaseCurrencyPerCurrency2"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SideRateBasisEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum SideRateBasisEnum {


    /**
     * The amount of the exchangedCurrency1 for one unit of baseCurrency.
     * 
     */
    @XmlEnumValue("Currency1PerBaseCurrency")
    CURRENCY_1_PER_BASE_CURRENCY("Currency1PerBaseCurrency"),

    /**
     * The amount of the baseCurrency for one unit of exchangedCurrency1.
     * 
     */
    @XmlEnumValue("BaseCurrencyPerCurrency1")
    BASE_CURRENCY_PER_CURRENCY_1("BaseCurrencyPerCurrency1"),

    /**
     * The amount of the exchangedCurrency2 for one unit of baseCurrency.
     * 
     */
    @XmlEnumValue("Currency2PerBaseCurrency")
    CURRENCY_2_PER_BASE_CURRENCY("Currency2PerBaseCurrency"),

    /**
     * The amount of the baseCurrency for one unit of exchangedCurrency2.
     * 
     */
    @XmlEnumValue("BaseCurrencyPerCurrency2")
    BASE_CURRENCY_PER_CURRENCY_2("BaseCurrencyPerCurrency2");
    private final String value;

    SideRateBasisEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SideRateBasisEnum fromValue(String v) {
        for (SideRateBasisEnum c: SideRateBasisEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
