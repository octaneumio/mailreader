
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OnGoingFeeTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OnGoingFeeTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="CommitmentFee"/>
 *     &lt;enumeration value="UtilizationFee"/>
 *     &lt;enumeration value="FacilityFee"/>
 *     &lt;enumeration value="LetterOfCreditFee"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OnGoingFeeTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum OnGoingFeeTypeEnum {


    /**
     * Calculated as a percentage of the unutilized portion of the facility.
     * 
     */
    @XmlEnumValue("CommitmentFee")
    COMMITMENT_FEE("CommitmentFee"),

    /**
     * Calculated as a percentage of the utilized portion of the facility. This fee type is subject to banding rules – different portions of the utilization amount may be subject to different percentages.
     * 
     */
    @XmlEnumValue("UtilizationFee")
    UTILIZATION_FEE("UtilizationFee"),

    /**
     * Calculated as a percentage of the global commitment amount of a facility.
     * 
     */
    @XmlEnumValue("FacilityFee")
    FACILITY_FEE("FacilityFee"),

    /**
     * An on-going fee charged for the issuance of a letter of credit to a named beneficiary, on behalf of the borrower against a facility. The fee calculation is based on the percentage of the notional value of the letter of credit.
     * 
     */
    @XmlEnumValue("LetterOfCreditFee")
    LETTER_OF_CREDIT_FEE("LetterOfCreditFee");
    private final String value;

    OnGoingFeeTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OnGoingFeeTypeEnum fromValue(String v) {
        for (OnGoingFeeTypeEnum c: OnGoingFeeTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
