
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OneOffFeeTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OneOffFeeTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="AmendmentFee"/>
 *     &lt;enumeration value="AssignmentFee"/>
 *     &lt;enumeration value="FacilityExtensionFee"/>
 *     &lt;enumeration value="FundingFee"/>
 *     &lt;enumeration value="BreakageFee"/>
 *     &lt;enumeration value="UpfrontFee"/>
 *     &lt;enumeration value="WaiverFee"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OneOffFeeTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum OneOffFeeTypeEnum {


    /**
     * A fee charged to the borrower for an amendment being made to the originally agreed credit agreement. The fee is based on a rate (as stated in the agreement) applied to the current commitment level.Calculated as a percentage of the unutilized portion of the facility.
     * 
     */
    @XmlEnumValue("AmendmentFee")
    AMENDMENT_FEE("AmendmentFee"),

    /**
     * Calculated as a percentage of the unutilized portion of the facility.
     * 
     */
    @XmlEnumValue("AssignmentFee")
    ASSIGNMENT_FEE("AssignmentFee"),

    /**
     * This fee represents any fee paid by the borrower to the syndicate lenders for extending an existing facility.
     * 
     */
    @XmlEnumValue("FacilityExtensionFee")
    FACILITY_EXTENSION_FEE("FacilityExtensionFee"),

    /**
     * Fee associated with the funding requirements for given facility.
     * 
     */
    @XmlEnumValue("FundingFee")
    FUNDING_FEE("FundingFee"),

    /**
     * Calculated as the cost of breaking financing on a loan contract which is repaid early.
     * 
     */
    @XmlEnumValue("BreakageFee")
    BREAKAGE_FEE("BreakageFee"),

    /**
     * This fee is also known as Participation Fee, Arrangement Fee etc. This fee represents compensation to the members of the lending syndicate (and sometimes to institutional investors as well) in return for their commitment of capital.
     * 
     */
    @XmlEnumValue("UpfrontFee")
    UPFRONT_FEE("UpfrontFee"),

    /**
     * This fee represents any fee paid by the borrower to the syndicate lenders / agent bank for accepting /processing waiver request. Waiver request is sent by the borrower to obtain approval from the syndicate lenders for any of their requirement, which is outside the terms of the agreement.
     * 
     */
    @XmlEnumValue("WaiverFee")
    WAIVER_FEE("WaiverFee");
    private final String value;

    OneOffFeeTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OneOffFeeTypeEnum fromValue(String v) {
        for (OneOffFeeTypeEnum c: OneOffFeeTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
