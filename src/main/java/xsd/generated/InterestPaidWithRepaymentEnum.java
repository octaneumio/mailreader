
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterestPaidWithRepaymentEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InterestPaidWithRepaymentEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="NoInterest"/>
 *     &lt;enumeration value="PayedOnShareAmount"/>
 *     &lt;enumeration value="PayedOnRepaymentAmount"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InterestPaidWithRepaymentEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum InterestPaidWithRepaymentEnum {


    /**
     * Interest is not payed with repayment.
     * 
     */
    @XmlEnumValue("NoInterest")
    NO_INTEREST("NoInterest"),

    /**
     * Interest is payed with repayment. Interest accrual amount is based on lender loan contract share amount.
     * 
     */
    @XmlEnumValue("PayedOnShareAmount")
    PAYED_ON_SHARE_AMOUNT("PayedOnShareAmount"),

    /**
     * Interest is payed with repayment. Interest accrual amount is based on lender share repayment amount.
     * 
     */
    @XmlEnumValue("PayedOnRepaymentAmount")
    PAYED_ON_REPAYMENT_AMOUNT("PayedOnRepaymentAmount");
    private final String value;

    InterestPaidWithRepaymentEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterestPaidWithRepaymentEnum fromValue(String v) {
        for (InterestPaidWithRepaymentEnum c: InterestPaidWithRepaymentEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
