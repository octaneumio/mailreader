
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SettlementPeriodDurationEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SettlementPeriodDurationEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Hourly"/>
 *     &lt;enumeration value="HalfHourly"/>
 *     &lt;enumeration value="QuarterHourly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SettlementPeriodDurationEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum SettlementPeriodDurationEnum {


    /**
     * Hourly duration applies.
     * 
     */
    @XmlEnumValue("Hourly")
    HOURLY("Hourly"),

    /**
     * Half-hourly duration applies.
     * 
     */
    @XmlEnumValue("HalfHourly")
    HALF_HOURLY("HalfHourly"),

    /**
     * Quarter-hourly duration applies.
     * 
     */
    @XmlEnumValue("QuarterHourly")
    QUARTER_HOURLY("QuarterHourly");
    private final String value;

    SettlementPeriodDurationEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SettlementPeriodDurationEnum fromValue(String v) {
        for (SettlementPeriodDurationEnum c: SettlementPeriodDurationEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
