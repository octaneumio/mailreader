
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PeriodEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PeriodEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="H"/>
 *     &lt;enumeration value="D"/>
 *     &lt;enumeration value="W"/>
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="Y"/>
 *     &lt;enumeration value="T"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PeriodEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum PeriodEnum {


    /**
     * Hour.
     * 
     */
    H,

    /**
     * Day.
     * 
     */
    D,

    /**
     * Week.
     * 
     */
    W,

    /**
     * Month.
     * 
     */
    M,

    /**
     * Year.
     * 
     */
    Y,

    /**
     * Term. The period commencing on the effective date and ending on the termination date. The T period always appears in association with periodMultiplier = 1, and the notation is intended for use in contexts where the interval thus qualified (e.g. accrual period, payment period, reset period, ...) spans the entire term of the trade.
     * 
     */
    T;

    public String value() {
        return name();
    }

    public static PeriodEnum fromValue(String v) {
        return valueOf(v);
    }

}
