
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OptionTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OptionTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Call"/>
 *     &lt;enumeration value="Forward"/>
 *     &lt;enumeration value="Put"/>
 *     &lt;enumeration value="Payer"/>
 *     &lt;enumeration value="Receiver"/>
 *     &lt;enumeration value="Straddle"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OptionTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum OptionTypeEnum {


    /**
     * A call option gives the holder the right to buy the underlying asset by a certain date for a certain price.
     * 
     */
    @XmlEnumValue("Call")
    CALL("Call"),

    /**
     * DEPRECATED value which will be removed in FpML-5-0 onwards A forward contract is an agreement to buy or sell the underlying asset at a certain future time for a certain price.
     * 
     */
    @XmlEnumValue("Forward")
    FORWARD("Forward"),

    /**
     * A put option gives the holder the right to sell the underlying asset by a certain date for a certain price.
     * 
     */
    @XmlEnumValue("Put")
    PUT("Put"),

    /**
     * A payer option
     * 
     */
    @XmlEnumValue("Payer")
    PAYER("Payer"),

    /**
     * A receiver option
     * 
     */
    @XmlEnumValue("Receiver")
    RECEIVER("Receiver"),

    /**
     * A straddle strategy.
     * 
     */
    @XmlEnumValue("Straddle")
    STRADDLE("Straddle");
    private final String value;

    OptionTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OptionTypeEnum fromValue(String v) {
        for (OptionTypeEnum c: OptionTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
