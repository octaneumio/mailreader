
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExerciseStyleEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExerciseStyleEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="American"/>
 *     &lt;enumeration value="Bermuda"/>
 *     &lt;enumeration value="European"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ExerciseStyleEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum ExerciseStyleEnum {


    /**
     * Option can be exercised on any date up to the expiry date.
     * 
     */
    @XmlEnumValue("American")
    AMERICAN("American"),

    /**
     * Option can be exercised on specified dates up to the expiry date.
     * 
     */
    @XmlEnumValue("Bermuda")
    BERMUDA("Bermuda"),

    /**
     * Option can only be exercised on the expiry date.
     * 
     */
    @XmlEnumValue("European")
    EUROPEAN("European");
    private final String value;

    ExerciseStyleEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExerciseStyleEnum fromValue(String v) {
        for (ExerciseStyleEnum c: ExerciseStyleEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
