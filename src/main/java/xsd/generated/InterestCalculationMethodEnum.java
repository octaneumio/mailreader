
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterestCalculationMethodEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InterestCalculationMethodEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="ProRataShare"/>
 *     &lt;enumeration value="FacilityPosition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InterestCalculationMethodEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum InterestCalculationMethodEnum {


    /**
     * Agent bank is making an interest payment based on the lender pro-rata share.
     * 
     */
    @XmlEnumValue("ProRataShare")
    PRO_RATA_SHARE("ProRataShare"),

    /**
     * Agent bank is making an interest payment based on the lender position throughout the period.
     * 
     */
    @XmlEnumValue("FacilityPosition")
    FACILITY_POSITION("FacilityPosition");
    private final String value;

    InterestCalculationMethodEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InterestCalculationMethodEnum fromValue(String v) {
        for (InterestCalculationMethodEnum c: InterestCalculationMethodEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
