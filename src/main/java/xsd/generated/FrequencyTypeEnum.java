
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FrequencyTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FrequencyTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Business"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FrequencyTypeEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum FrequencyTypeEnum {


    /**
     * Day is the unit of frequency.
     * 
     */
    @XmlEnumValue("Day")
    DAY("Day"),

    /**
     * TBD
     * 
     */
    @XmlEnumValue("Business")
    BUSINESS("Business");
    private final String value;

    FrequencyTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FrequencyTypeEnum fromValue(String v) {
        for (FrequencyTypeEnum c: FrequencyTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
