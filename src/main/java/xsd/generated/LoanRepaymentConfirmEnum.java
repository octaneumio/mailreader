
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanRepaymentConfirmEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LoanRepaymentConfirmEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="AcceptInFull"/>
 *     &lt;enumeration value="PartiallyAccept"/>
 *     &lt;enumeration value="Deny"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LoanRepaymentConfirmEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum LoanRepaymentConfirmEnum {


    /**
     * Lender is accepting the repayment in full.
     * 
     */
    @XmlEnumValue("AcceptInFull")
    ACCEPT_IN_FULL("AcceptInFull"),

    /**
     * Lender is partially accepting the repayment.
     * 
     */
    @XmlEnumValue("PartiallyAccept")
    PARTIALLY_ACCEPT("PartiallyAccept"),

    /**
     * Lender is denying the repayment.
     * 
     */
    @XmlEnumValue("Deny")
    DENY("Deny");
    private final String value;

    LoanRepaymentConfirmEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LoanRepaymentConfirmEnum fromValue(String v) {
        for (LoanRepaymentConfirmEnum c: LoanRepaymentConfirmEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
