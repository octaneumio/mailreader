
package xsd.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FraDiscountingEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FraDiscountingEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="ISDA"/>
 *     &lt;enumeration value="AFMA"/>
 *     &lt;enumeration value="NONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FraDiscountingEnum", namespace = "http://www.fpml.org/2009/FpML-4-6")
@XmlEnum
public enum FraDiscountingEnum {


    /**
     * "FRA Discounting" per the ISDA Definitions will apply.
     * 
     */
    ISDA,

    /**
     * FRA discounting per the Australian Financial Markets Association (AFMA) OTC Financial Product Conventions will apply.
     * 
     */
    AFMA,

    /**
     * No discounting will apply.
     * 
     */
    NONE;

    public String value() {
        return name();
    }

    public static FraDiscountingEnum fromValue(String v) {
        return valueOf(v);
    }

}
