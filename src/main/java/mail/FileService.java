package mail;

import javax.mail.internet.MimeBodyPart;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by Karthick on 15/9/18.
 */
public class FileService {

    /**
     * Save the attachment to the local machine
     * @param attachment map
     */

    public String saveFileFromEmailAttachment(Map attachment) {
        try {
            System.out.println("Saving attachments");
            File dir = createAndGetDirectory();
            String fileName = attachment.get("fileName").toString();
            File toFile = new File(dir, fileName);
            MimeBodyPart mimeBodyPart = (MimeBodyPart) attachment.get("mimeBodyPart");
            mimeBodyPart.saveFile(toFile);
            return toFile.getAbsolutePath();
        }
        catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    /**
     * Create the directory which contains all the attachments from the mail
     */
    private static File createAndGetDirectory() {
        File file = new File("attachments-read");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    /**
     * Read and return the file content as string
     *
     * @param fileName file name which is to be read
     * @return string
     */
    public String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    /**
     * Move processed file to attachments-processed directory
     *
     * @param location destination directory
     * @param file content which is to be moved
     * @return string
     */
    public void moveFile(String location, File file) {
        File locationFile = new File(location);
        if (!locationFile.exists()) {
            locationFile.mkdir();
        }
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        if (file.renameTo(new File(location + "/" + file.getName()+"_"+ timestamp))) {
            System.out.println("File is moved successfully!");
        } else {
            System.out.println("File is failed to move!");
        }
    }
}
