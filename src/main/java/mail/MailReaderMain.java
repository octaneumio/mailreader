package mail;

import javax.mail.Session;

/**
 * Created by Karthick on 8/9/18.
 */
public class MailReaderMain {

    /**
     * Entry point for mail connector service
     */
    public static void main(String[] args)  {
        try {
            System.out.println("[MailReaderMain] [main] [Entry]");
            MailConnectorService mailConnectorService = new MailConnectorService();

            // get session
            Session session = mailConnectorService.getSession();
            if(session != null) {
                MailReaderService mailReaderService = new MailReaderService();
                mailReaderService.readEmail(session);
            }
            System.out.println("[MailReaderMain] [main] [Exit]");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
