package mail;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xml.generated.interest.payment.xsd.FpMLType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by Karthick on 15/9/18.
 */
public class XMLService {


    DBService dbService;
    FileService fileService;

    /**
     * Xml's are converted to xsd and then those xsd are converted to model class.
     * @param filePath filepath of the xml
     * @param fileName file name of the xml
     */

    public void parseXml(String filePath, String fileName) {
        try {
            System.out.println("Parsing XML begin from" + filePath);
            initAllService();

            // Saved xml file path is parsed to know which schema class it belongs to.
            String schemaClass = getSchemaClass(filePath);

            // Then the exact package of the class is identified for unmarshalling
            String unMarshallClass = getUnmarshallClass(schemaClass);
            Class aclass = Class.forName(unMarshallClass);
            File file = new File(filePath);

            // Unmarshalling process starts
            JAXBContext jaxbContext = JAXBContext.newInstance(aclass);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Object FpML = (Object) unmarshaller.unmarshal(file);
            System.out.println("Unmarshall success");

            // Printing objects to console once the target class is found
            if(FpML instanceof FpMLType) {
                FpMLType FpMLType= (FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy());
            } else if(FpML instanceof xml.generated.loan.draw.down.FpMLType) {
                xml.generated.loan.draw.down.FpMLType FpMLType= (xml.generated.loan.draw.down.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy());
            } else if(FpML instanceof xml.generated.rollover.notice.xsd.FpMLType) {
                xml.generated.rollover.notice.xsd.FpMLType FpMLType= (xml.generated.rollover.notice.xsd.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.issuance.notice.FpMLType) {
                xml.generated.loan.issuance.notice.FpMLType FpMLType= (xml.generated.loan.issuance.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.amendment.notice.FpMLType) {
                xml.generated.loan.amendment.notice.FpMLType FpMLType= (xml.generated.loan.amendment.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.pricing.change.FpMLType) {
                xml.generated.loan.pricing.change.FpMLType FpMLType= (xml.generated.loan.pricing.change.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.balance.notice.FpMLType) {
                xml.generated.loan.balance.notice.FpMLType FpMLType= (xml.generated.loan.balance.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.termination.notice.FpMLType) {
                xml.generated.loan.termination.notice.FpMLType FpMLType= (xml.generated.loan.termination.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            } else if(FpML instanceof xml.generated.loan.repayment.notice.FpMLType) {
                xml.generated.loan.repayment.notice.FpMLType FpMLType= (xml.generated.loan.repayment.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy());
            } else if(FpML instanceof xml.generated.loan.pricing.change.notice.FpMLType) {
                xml.generated.loan.pricing.change.notice.FpMLType FpMLType= (xml.generated.loan.pricing.change.notice.FpMLType) FpML;
                System.out.println(">>>>> "+FpMLType.getHeader().getSentBy().getValue());
            }

            // save to db
            dbService.saveToDB(fileService.readFile(filePath),fileName);

            // move file to processed folder
            fileService.moveFile("attachments-processed",file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Identify the schema class mapped with the xml which we got from the attachment
     *
     * @param path file path of the attachment
     * @return the response message wrapper
     */
    private String getSchemaClass(String path) {
        try {
            //Get Document Builder
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            //Build Document
            Document document = builder.parse(new File(path));

            //Normalize the XML Structure; It's just too important !!
            document.getDocumentElement().normalize();

            //Here comes the root node
            Element root = document.getDocumentElement();
            System.out.println(root.getNodeName());

            NodeList nList = document.getElementsByTagName("FpML");
            for (int temp = 0; temp < nList.getLength(); temp++)
            {
                Node node = nList.item(temp);
                System.out.println("");    //Just a separator
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    //Print each employee's detail
                    Element eElement = (Element) node;
                    return eElement.getAttribute("xsi:type");
                }
            }
        } catch (Exception e) {
            System.out.println("Error gettign schema class " +e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Get the exact class which should used for unmarshalling
     * @param schemaClass class mapped in the xml
     * @return string
     */
    private String getUnmarshallClass(String schemaClass) {
        String unMarshallClass = "";
        if("DrawdownNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "FpMLType";
        } else if("InterestPaymentNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.interest.payment.xsd.FpMLType";
        } else if("RolloverNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.rollover.notice.xsd.FpMLType";
        } else if("LcAmendmentNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.amendment.notice.FpMLType";
        } else if("LcIssuanceNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.issuance.notice.FpMLType";
        } else if("PricingChangeNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.pricing.change.FpMLType";
        } else if("LcBalanceNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.balance.notice.FpMLType";
        } else if("LcTerminationNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.termination.notice.FpMLType";
        } else if("RepaymentNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xml.generated.loan.repayment.notice.FpMLType";
        } else if("PricingChangeNotice".equalsIgnoreCase(schemaClass)) {
            unMarshallClass = "xmxml.generated.loan.pricing.change.notice.FpMLTypee";
        }
        return unMarshallClass;
    }

    /**
     * Initialize all services
     */
    private void initAllService() {
        System.out.println("Iniatializing XML services");
        dbService = new DBService();
        fileService = new FileService();
    }

}
