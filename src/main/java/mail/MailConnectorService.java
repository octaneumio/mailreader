package mail;


import util.CommonUtil;

import javax.mail.Session;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;
import java.util.Properties;

/**
 * Created by Ideas2it-Karthick on 14/9/18.
 */
public class MailConnectorService {

    /**
     * Get session for connecting office
     *
     * @return session
     */
    public Session getSession() {
        System.out.println("[MailConnectorService] [getSession] [Entry] [Get Session]");
        Session session = null;
        Properties properties = new Properties();
        try {
            // load properties file
            properties.load(CommonUtil.loadPropertyFile(this.getClass().getClassLoader()));
            properties.setProperty("mail.store.PROTOCOL", properties.getProperty("PROTOCOL"));
            if(properties.getProperty("ENCRYPTION_TYPE").length() > 0 && !properties.getProperty("ENCRYPTION_TYPE").equalsIgnoreCase("none")) {
                properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                properties.setProperty("mail.imap.socketFactory.fallback", "false");
                properties.setProperty("mail.imap.ssl.enable", "true");
                if(properties.getProperty("ENCRYPTION_TYPE").equalsIgnoreCase("tls")) {
                    properties.setProperty("mail.imap.starttls.enable", "true");
                }
            }
            session = Session.getInstance(properties, null);
//            session.setDebug(true);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("[MailConnectorService] [getSession] [Exit] [Get Session Failed] "+ex.getMessage());
        }
        System.out.println("[MailConnectorService] [getSession] [Exit] [Get Session]");

        return session;
    }
}
