package mail;

import com.sun.mail.imap.IMAPFolder;
import util.CommonUtil;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Karthick on 15/9/18.
 */
public class MailReaderService {

    DBService dbService;
    XMLService xmlService;
    FileService fileService;

    /**
     * Read unread Inbox messages
     * @param session Session for connecting office
     */
    public void readEmail(Session session) {
        Properties properties = new Properties();
        try {
            System.out.println("[MailReaderService] [readEmail] [Entry]");

            // init all necessary services
            initAllService();

            // load properties file
            properties.load(CommonUtil.loadPropertyFile(this.getClass().getClassLoader()));

            // connect office
            System.out.println("[MailReaderService] [readEmail] [Get store from session and connecting outlook]");
            Store store = session.getStore("imaps");
            store.connect(properties.getProperty("HOST"), properties.getProperty("USER"), properties.getProperty("PASSWORD"));

            // Get inbox folder and its messages
            System.out.println("[MailReaderService] [readEmail] [Connecting outlook and fetching messages]");
            IMAPFolder inbox = (IMAPFolder) store.getFolder("INBOX");
            // Open the inbox using store
            inbox.open(Folder.READ_WRITE);


            // Get the messages which is unread in the Inbox
            Message messages[] = inbox.search(new FlagTerm(new Flags(
                    Flags.Flag.SEEN), false));

            // Process XML in the mail
            processXMLInMail(messages);

            // Close all mail connections
            inbox.close(false);
            store.close();
            System.out.println("[MailReaderService] [readEmail] [Exit]");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check and process the xml attachments in the message
     * @param messages Unread messages
     */
    private void processXMLInMail(Message[] messages) {
        System.out.println("[MailReaderService] [processXMLInMail] [Entry]");

        ExecutorService executor = Executors.newFixedThreadPool(5);
        try {
            Properties properties = new Properties();

            // load properties file
            properties.load(CommonUtil.loadPropertyFile(this.getClass().getClassLoader()));

            int maxReadNumber = Integer.parseInt(properties.getProperty("maxReadNumber"));
            int total = messages.length > maxReadNumber ? maxReadNumber : messages.length;
            boolean ascending = false;

            System.out.println("[MailReaderService] [processXMLInMail] [Total Unread Email] " + messages.length);

            // Without using threads
            /*for (int index = 0; index < total; index++) {
                readMailAttachments(messages, index,inbox);
            }*/

            // Using threads
            List<Future<?>> futures = new ArrayList<>();
            for (int index = 0; index < total; index++) {
                int finalIndex = index;
                Future<?> future = executor.submit(() -> {
                    readMailAttachments(messages, finalIndex);
                });
                futures.add(future);
            }
            try {
                for (Future<?> future : futures) {
                    System.out.println("[MailReaderService] [processXMLInMail] [future-thread] Process");
                    future.get();
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            System.out.println("[MailReaderService] [processXMLInMail] [Exit]");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error processing messages");
        }
    }

    private void readMailAttachments(Message[] messages, int index) {
        try {

            Message message = messages[index];
            String subject = message.getSubject();

            // set to seen after all process is over
            message.setFlag(Flags.Flag.SEEN, true);
            System.out.println("[MailReaderService] [readMailAttachments] [Processing mail with subject] "+subject);
            Map<String,Object>  output = processMultipart((Multipart) message.getContent());
            Object[] keys = output.keySet().toArray();
            for (int i = 0; i < keys.length; i++) {
                System.out.println("[MailReaderService] [readMailAttachments] [Processing attachments]");
                if (keys[i].toString().equalsIgnoreCase("attachments")) {
                    List attachments = (List) output.get("attachments");
                    for (int j = 0; j < attachments.size(); j++) {
                        Map attachment = (Map) attachments.get(j);

                        // You can save attachments
                        String filePath = fileService.saveFileFromEmailAttachment(attachment);
                        System.out.println("[MailReaderService] [readMailAttachments] Saving "+attachment.get("fileName")+" to " + filePath);

                        // parse XML
                        xmlService.parseXml(filePath,attachment.get("fileName").toString());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Init all services required for reading mails
     */
    private void initAllService() {
        System.out.println("[MailReaderService] [initAllService] [Iniatializing Mail reader services]");
        dbService = new DBService();
        fileService = new FileService();
        xmlService = new XMLService();
    }

    /**
     * Multipart is the container which contains the body part. Iterate the multipart and get the
     * body part part content
     *
     * @param multipart Multipart object
     * @return attachment map
     */
    private Map<String,Object> processMultipart(Multipart multipart) throws Exception {
        System.out.println("[MailReaderService] [processMultipart] [Process multi part]");

        Map<String,Object> output = new HashMap<String,Object>();
        List<Map<String,Object>> attachments = new ArrayList<Map<String,Object>>();
        for(int i = 0; i < multipart.getCount(); i++) {
            // Processing body part for checking only xml attachments.
            BodyPart bodyPart = multipart.getBodyPart(i);
            String fileType = bodyPart.getContentType();
            if ("text/xml".equalsIgnoreCase(fileType) ||
                    fileType.substring(0, fileType.indexOf(";")).equalsIgnoreCase("text/xml")) {
                Map<String,Object> result = processBodyPart(bodyPart);
                if (result != null) {
                    if (result.containsKey("type") && result.get("type").toString().equalsIgnoreCase("attachment")) {
                        attachments.add(result);
                    }
                /*if (result.containsKey("attachments")) {
                    List thisAttachments = (List) result.get("attachments");
                    for (int i2 = 0; i2 < thisAttachments.size(); i2++) {
                        attachments.add(thisAttachments.get(i2));
                    }
                }*/
                }
            }
        }
        output.put("attachments", attachments);
        return output;
    }

    /**
     * Check the attachment details contained in the body part of the mail
     *
     * @param bodyPart bodypart object
     * @return file details map
     */

    private Map<String,Object> processBodyPart(BodyPart bodyPart) throws Exception {
        // Check for file type.
        System.out.println("[MailReaderService] [processBodyPart] [Entry] Process body part");
        if(Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) && bodyPart.getFileName() != null) {
            try {
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("type", "attachment");
                map.put("fileName", bodyPart.getFileName());
                String fileType = bodyPart.getContentType();
                map.put("fileType", fileType.contains(":") ? fileType.substring(0, fileType.indexOf(";")) : fileType);
                map.put("mimeBodyPart", bodyPart);
                return map;
            }
            catch (Exception ex) {
                System.out.println("[MailReaderService] [processBodyPart] Error_Content=" + bodyPart.getContentType());
                ex.printStackTrace();
            }
        }/*
        else if(bodyPart.getContentType().contains("multipart")) {
            Map o = processMultipart((Multipart) bodyPart.getContent());
            return o;
        }*/
        return null;
    }
}

