package mail;

import com.fpml.rest.model.FpmlMessageDTO;
import util.CommonUtil;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Karthick on 15/9/18.
 */
public class DBService {

    /**
     * Save file name and the content to Database
     *
     * @param content file content
     * @param content file name
     */
    public void saveToDB(String content, String fileName) {
        try {
            Connection conn = getConnection();
            if (conn != null) {
                System.out.println("Connected");
                String sql = "INSERT INTO mailxml (filename, xmlcontent) VALUES (?, ?)";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, fileName);
                statement.setString(2, content);
                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("Inserted successfully!");
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get Db connection
     */
    private Connection getConnection() {
        Properties properties = new Properties();
        try {
            // load properties file
            properties.load(CommonUtil.loadPropertyFile(this.getClass().getClassLoader()));
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/mailreader","root","root");
        } catch (SQLException e) {
            e.printStackTrace();
        }/* catch (IOException e) {
            e.printStackTrace();
        }*/ catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void getAllMessages(Date startDate, Date endDate, String status) {
        try {
            Connection connection = getConnection();
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("select record_id,message_id " +
                        "from fpml_audit where message_created_on = ? and notice_date = ? and message_status = ?");

                statement.setDate(1, (java.sql.Date) startDate);
                statement.setDate(1, (java.sql.Date) endDate);
                statement.setString(1, status);
                ResultSet resultSet = statement.executeQuery();
                while ( resultSet.next() ) {

                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void getAllMessages(Date startDate, Date endDate) {
        try {
            Connection connection = getConnection();
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("select record_id,message_id " +
                        "from fpml_audit where message_created_on = ? and notice_date = ?");

                statement.setDate(1, (java.sql.Date) startDate);
                statement.setDate(1, (java.sql.Date) endDate);
                ResultSet resultSet = statement.executeQuery();
                while ( resultSet.next() ) {

                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public FpmlMessageDTO getMessage(int msgId) {
        FpmlMessageDTO fpmlMessageDTO = new FpmlMessageDTO();
        try {
            Connection connection = getConnection();
            if (connection != null) {
                System.out.println("Connected");
                PreparedStatement statement = connection.prepareStatement("select CREATED_ON as createdOn," +
                        "record_id as recordId,message as message from fpm_message where message_id = ?");
                statement.setInt(1, msgId);
                ResultSet resultSet = statement.executeQuery();
                while ( resultSet.next() ) {
                    Blob b = resultSet.getBlob("message");
                    fpmlMessageDTO.setCreatedOn(resultSet.getDate("createdOn"));
                    fpmlMessageDTO.setRecordId(resultSet.getString("recordId"));
                    fpmlMessageDTO.setMessage(new String(b.getBytes(1l, (int) b.length())));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return fpmlMessageDTO;
    }
}
