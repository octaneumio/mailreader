package jaxwsExample.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class MessageWs {

  @WebMethod
  public String sendMessage(@WebParam(name="user") String user , @WebParam(name="message") String message) {
    System.out.println("==============="+user);
    if (user == null) {
      return "Hello";
    }
    return "Hello, " + user + "! Your message : "+message;
  }
}