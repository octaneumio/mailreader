
package xml.generated.interest.payment.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xml.generated.interest.payment.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FpML_QNAME = new QName("http://www.fpml.org/2009/FpML-4-6", "FpML");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xml.generated.interest.payment.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FpMLType }
     * 
     */
    public FpMLType createFpMLType() {
        return new FpMLType();
    }

    /**
     * Create an instance of {@link FacilitySummaryType }
     * 
     */
    public FacilitySummaryType createFacilitySummaryType() {
        return new FacilitySummaryType();
    }

    /**
     * Create an instance of {@link InterestAccrualPeriodType }
     * 
     */
    public InterestAccrualPeriodType createInterestAccrualPeriodType() {
        return new InterestAccrualPeriodType();
    }

    /**
     * Create an instance of {@link TenorType }
     * 
     */
    public TenorType createTenorType() {
        return new TenorType();
    }

    /**
     * Create an instance of {@link LenderPartyReferenceType }
     * 
     */
    public LenderPartyReferenceType createLenderPartyReferenceType() {
        return new LenderPartyReferenceType();
    }

    /**
     * Create an instance of {@link IdentifierType }
     * 
     */
    public IdentifierType createIdentifierType() {
        return new IdentifierType();
    }

    /**
     * Create an instance of {@link LenderLoanContractPeriodType }
     * 
     */
    public LenderLoanContractPeriodType createLenderLoanContractPeriodType() {
        return new LenderLoanContractPeriodType();
    }

    /**
     * Create an instance of {@link ShareInterestAccrualAmountType }
     * 
     */
    public ShareInterestAccrualAmountType createShareInterestAccrualAmountType() {
        return new ShareInterestAccrualAmountType();
    }

    /**
     * Create an instance of {@link PartyType }
     * 
     */
    public PartyType createPartyType() {
        return new PartyType();
    }

    /**
     * Create an instance of {@link BorrowerPartyReferenceType }
     * 
     */
    public BorrowerPartyReferenceType createBorrowerPartyReferenceType() {
        return new BorrowerPartyReferenceType();
    }

    /**
     * Create an instance of {@link ShareLoanContractAmountType }
     * 
     */
    public ShareLoanContractAmountType createShareLoanContractAmountType() {
        return new ShareLoanContractAmountType();
    }

    /**
     * Create an instance of {@link InterestPaymentType }
     * 
     */
    public InterestPaymentType createInterestPaymentType() {
        return new InterestPaymentType();
    }

    /**
     * Create an instance of {@link InterestRatePeriodType }
     * 
     */
    public InterestRatePeriodType createInterestRatePeriodType() {
        return new InterestRatePeriodType();
    }

    /**
     * Create an instance of {@link AgentBankPartyReferenceType }
     * 
     */
    public AgentBankPartyReferenceType createAgentBankPartyReferenceType() {
        return new AgentBankPartyReferenceType();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link FloatingRateIndexType }
     * 
     */
    public FloatingRateIndexType createFloatingRateIndexType() {
        return new FloatingRateIndexType();
    }

    /**
     * Create an instance of {@link ContractIdType }
     * 
     */
    public ContractIdType createContractIdType() {
        return new ContractIdType();
    }

    /**
     * Create an instance of {@link DealSummaryType }
     * 
     */
    public DealSummaryType createDealSummaryType() {
        return new DealSummaryType();
    }

    /**
     * Create an instance of {@link MessageIdType }
     * 
     */
    public MessageIdType createMessageIdType() {
        return new MessageIdType();
    }

    /**
     * Create an instance of {@link InstrumentIdType }
     * 
     */
    public InstrumentIdType createInstrumentIdType() {
        return new InstrumentIdType();
    }

    /**
     * Create an instance of {@link PartyReferenceType }
     * 
     */
    public PartyReferenceType createPartyReferenceType() {
        return new PartyReferenceType();
    }

    /**
     * Create an instance of {@link LoanContractSummaryType }
     * 
     */
    public LoanContractSummaryType createLoanContractSummaryType() {
        return new LoanContractSummaryType();
    }

    /**
     * Create an instance of {@link InterestAccrualScheduleType }
     * 
     */
    public InterestAccrualScheduleType createInterestAccrualScheduleType() {
        return new InterestAccrualScheduleType();
    }

    /**
     * Create an instance of {@link HeaderType }
     * 
     */
    public HeaderType createHeaderType() {
        return new HeaderType();
    }

    /**
     * Create an instance of {@link ShareAmountType }
     * 
     */
    public ShareAmountType createShareAmountType() {
        return new ShareAmountType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FpMLType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fpml.org/2009/FpML-4-6", name = "FpML")
    public JAXBElement<FpMLType> createFpML(FpMLType value) {
        return new JAXBElement<FpMLType>(_FpML_QNAME, FpMLType.class, null, value);
    }

}
