
package xml.generated.interest.payment.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for interestAccrualPeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interestAccrualPeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shareInterestAccrualAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareInterestAccrualAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interestAccrualPeriodType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "startDate",
    "endDate",
    "shareInterestAccrualAmount"
})
public class InterestAccrualPeriodType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String startDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String endDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ShareInterestAccrualAmountType shareInterestAccrualAmount;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the shareInterestAccrualAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareInterestAccrualAmountType }
     *     
     */
    public ShareInterestAccrualAmountType getShareInterestAccrualAmount() {
        return shareInterestAccrualAmount;
    }

    /**
     * Sets the value of the shareInterestAccrualAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareInterestAccrualAmountType }
     *     
     */
    public void setShareInterestAccrualAmount(ShareInterestAccrualAmountType value) {
        this.shareInterestAccrualAmount = value;
    }

}
