
package xml.generated.interest.payment.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for interestRatePeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interestRatePeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rateFixingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="floatingRateIndex" type="{http://www.fpml.org/2009/FpML-4-6}floatingRateIndexType"/>
 *         &lt;element name="interestRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="margin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="allInRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="interestDayBasis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tenor" type="{http://www.fpml.org/2009/FpML-4-6}tenorType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interestRatePeriodType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "rateFixingDate",
    "startDate",
    "endDate",
    "floatingRateIndex",
    "interestRate",
    "margin",
    "allInRate",
    "interestDayBasis",
    "tenor"
})
public class InterestRatePeriodType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String rateFixingDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String startDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String endDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected FloatingRateIndexType floatingRateIndex;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String interestRate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String margin;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String allInRate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String interestDayBasis;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected TenorType tenor;

    /**
     * Gets the value of the rateFixingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateFixingDate() {
        return rateFixingDate;
    }

    /**
     * Sets the value of the rateFixingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateFixingDate(String value) {
        this.rateFixingDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the floatingRateIndex property.
     * 
     * @return
     *     possible object is
     *     {@link FloatingRateIndexType }
     *     
     */
    public FloatingRateIndexType getFloatingRateIndex() {
        return floatingRateIndex;
    }

    /**
     * Sets the value of the floatingRateIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatingRateIndexType }
     *     
     */
    public void setFloatingRateIndex(FloatingRateIndexType value) {
        this.floatingRateIndex = value;
    }

    /**
     * Gets the value of the interestRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestRate() {
        return interestRate;
    }

    /**
     * Sets the value of the interestRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestRate(String value) {
        this.interestRate = value;
    }

    /**
     * Gets the value of the margin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMargin() {
        return margin;
    }

    /**
     * Sets the value of the margin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMargin(String value) {
        this.margin = value;
    }

    /**
     * Gets the value of the allInRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllInRate() {
        return allInRate;
    }

    /**
     * Sets the value of the allInRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllInRate(String value) {
        this.allInRate = value;
    }

    /**
     * Gets the value of the interestDayBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestDayBasis() {
        return interestDayBasis;
    }

    /**
     * Sets the value of the interestDayBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestDayBasis(String value) {
        this.interestDayBasis = value;
    }

    /**
     * Gets the value of the tenor property.
     * 
     * @return
     *     possible object is
     *     {@link TenorType }
     *     
     */
    public TenorType getTenor() {
        return tenor;
    }

    /**
     * Sets the value of the tenor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TenorType }
     *     
     */
    public void setTenor(TenorType value) {
        this.tenor = value;
    }

}
