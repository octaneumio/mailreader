
package xml.generated.interest.payment.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FpMLType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FpMLType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.fpml.org/2009/FpML-4-6}headerType"/>
 *         &lt;element name="noticeDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentBankPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}agentBankPartyReferenceType"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="lenderPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}lenderPartyReferenceType"/>
 *         &lt;element name="dealSummary" type="{http://www.fpml.org/2009/FpML-4-6}dealSummaryType"/>
 *         &lt;element name="facilitySummary" type="{http://www.fpml.org/2009/FpML-4-6}facilitySummaryType"/>
 *         &lt;element name="loanContractSummary" type="{http://www.fpml.org/2009/FpML-4-6}loanContractSummaryType"/>
 *         &lt;element name="interestPayment" type="{http://www.fpml.org/2009/FpML-4-6}interestPaymentType"/>
 *         &lt;element name="interestAccrualSchedule" type="{http://www.fpml.org/2009/FpML-4-6}interestAccrualScheduleType"/>
 *         &lt;element name="party" type="{http://www.fpml.org/2009/FpML-4-6}partyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FpMLType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "header",
    "noticeDate",
    "agentBankPartyReference",
    "borrowerPartyReference",
    "lenderPartyReference",
    "dealSummary",
    "facilitySummary",
    "loanContractSummary",
    "interestPayment",
    "interestAccrualSchedule",
    "party"
})
@XmlRootElement(name="FpML", namespace = "http://www.fpml.org/2009/FpML-4-6")
public class FpMLType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected HeaderType header;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String noticeDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected AgentBankPartyReferenceType agentBankPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LenderPartyReferenceType lenderPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected DealSummaryType dealSummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected FacilitySummaryType facilitySummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LoanContractSummaryType loanContractSummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected InterestPaymentType interestPayment;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected InterestAccrualScheduleType interestAccrualSchedule;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6")
    protected List<PartyType> party;
    @XmlAttribute(name = "version")
    protected String version;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the noticeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoticeDate() {
        return noticeDate;
    }

    /**
     * Sets the value of the noticeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoticeDate(String value) {
        this.noticeDate = value;
    }

    /**
     * Gets the value of the agentBankPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    public AgentBankPartyReferenceType getAgentBankPartyReference() {
        return agentBankPartyReference;
    }

    /**
     * Sets the value of the agentBankPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    public void setAgentBankPartyReference(AgentBankPartyReferenceType value) {
        this.agentBankPartyReference = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the lenderPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link LenderPartyReferenceType }
     *     
     */
    public LenderPartyReferenceType getLenderPartyReference() {
        return lenderPartyReference;
    }

    /**
     * Sets the value of the lenderPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link LenderPartyReferenceType }
     *     
     */
    public void setLenderPartyReference(LenderPartyReferenceType value) {
        this.lenderPartyReference = value;
    }

    /**
     * Gets the value of the dealSummary property.
     * 
     * @return
     *     possible object is
     *     {@link DealSummaryType }
     *     
     */
    public DealSummaryType getDealSummary() {
        return dealSummary;
    }

    /**
     * Sets the value of the dealSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealSummaryType }
     *     
     */
    public void setDealSummary(DealSummaryType value) {
        this.dealSummary = value;
    }

    /**
     * Gets the value of the facilitySummary property.
     * 
     * @return
     *     possible object is
     *     {@link FacilitySummaryType }
     *     
     */
    public FacilitySummaryType getFacilitySummary() {
        return facilitySummary;
    }

    /**
     * Sets the value of the facilitySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilitySummaryType }
     *     
     */
    public void setFacilitySummary(FacilitySummaryType value) {
        this.facilitySummary = value;
    }

    /**
     * Gets the value of the loanContractSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractSummaryType }
     *     
     */
    public LoanContractSummaryType getLoanContractSummary() {
        return loanContractSummary;
    }

    /**
     * Sets the value of the loanContractSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractSummaryType }
     *     
     */
    public void setLoanContractSummary(LoanContractSummaryType value) {
        this.loanContractSummary = value;
    }

    /**
     * Gets the value of the interestPayment property.
     * 
     * @return
     *     possible object is
     *     {@link InterestPaymentType }
     *     
     */
    public InterestPaymentType getInterestPayment() {
        return interestPayment;
    }

    /**
     * Sets the value of the interestPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestPaymentType }
     *     
     */
    public void setInterestPayment(InterestPaymentType value) {
        this.interestPayment = value;
    }

    /**
     * Gets the value of the interestAccrualSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link InterestAccrualScheduleType }
     *     
     */
    public InterestAccrualScheduleType getInterestAccrualSchedule() {
        return interestAccrualSchedule;
    }

    /**
     * Sets the value of the interestAccrualSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestAccrualScheduleType }
     *     
     */
    public void setInterestAccrualSchedule(InterestAccrualScheduleType value) {
        this.interestAccrualSchedule = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getParty() {
        if (party == null) {
            party = new ArrayList<PartyType>();
        }
        return this.party;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
