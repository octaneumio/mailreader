
package xml.generated.interest.payment.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for amountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="amountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shareAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "amountType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "shareAmount"
})
public class AmountType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ShareAmountType shareAmount;

    /**
     * Gets the value of the shareAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareAmountType }
     *     
     */
    public ShareAmountType getShareAmount() {
        return shareAmount;
    }

    /**
     * Sets the value of the shareAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareAmountType }
     *     
     */
    public void setShareAmount(ShareAmountType value) {
        this.shareAmount = value;
    }

}
