
package xml.generated.interest.payment.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for interestAccrualScheduleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interestAccrualScheduleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interestRatePeriod" type="{http://www.fpml.org/2009/FpML-4-6}interestRatePeriodType"/>
 *         &lt;element name="lenderLoanContractPeriod" type="{http://www.fpml.org/2009/FpML-4-6}lenderLoanContractPeriodType"/>
 *         &lt;element name="interestAccrualPeriod" type="{http://www.fpml.org/2009/FpML-4-6}interestAccrualPeriodType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interestAccrualScheduleType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "interestRatePeriod",
    "lenderLoanContractPeriod",
    "interestAccrualPeriod"
})
public class InterestAccrualScheduleType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected InterestRatePeriodType interestRatePeriod;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LenderLoanContractPeriodType lenderLoanContractPeriod;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected InterestAccrualPeriodType interestAccrualPeriod;

    /**
     * Gets the value of the interestRatePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link InterestRatePeriodType }
     *     
     */
    public InterestRatePeriodType getInterestRatePeriod() {
        return interestRatePeriod;
    }

    /**
     * Sets the value of the interestRatePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestRatePeriodType }
     *     
     */
    public void setInterestRatePeriod(InterestRatePeriodType value) {
        this.interestRatePeriod = value;
    }

    /**
     * Gets the value of the lenderLoanContractPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link LenderLoanContractPeriodType }
     *     
     */
    public LenderLoanContractPeriodType getLenderLoanContractPeriod() {
        return lenderLoanContractPeriod;
    }

    /**
     * Sets the value of the lenderLoanContractPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link LenderLoanContractPeriodType }
     *     
     */
    public void setLenderLoanContractPeriod(LenderLoanContractPeriodType value) {
        this.lenderLoanContractPeriod = value;
    }

    /**
     * Gets the value of the interestAccrualPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link InterestAccrualPeriodType }
     *     
     */
    public InterestAccrualPeriodType getInterestAccrualPeriod() {
        return interestAccrualPeriod;
    }

    /**
     * Sets the value of the interestAccrualPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestAccrualPeriodType }
     *     
     */
    public void setInterestAccrualPeriod(InterestAccrualPeriodType value) {
        this.interestAccrualPeriod = value;
    }

}
