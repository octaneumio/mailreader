
package xml.generated.interest.payment.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for floatingRateIndexType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="floatingRateIndexType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="floatingRateIndexScheme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "floatingRateIndexType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "value"
})
public class FloatingRateIndexType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "floatingRateIndexScheme")
    protected String floatingRateIndexScheme;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the floatingRateIndexScheme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloatingRateIndexScheme() {
        return floatingRateIndexScheme;
    }

    /**
     * Sets the value of the floatingRateIndexScheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloatingRateIndexScheme(String value) {
        this.floatingRateIndexScheme = value;
    }

}
