
package xml.generated.loan.issuance.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for letterOfCreditType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="letterOfCreditType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lcSummary" type="{http://www.fpml.org/2009/FpML-4-6}lcSummaryType"/>
 *         &lt;element name="lcType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lcPurpose" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="beneficiaryPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}beneficiaryPartyReferenceType"/>
 *         &lt;element name="originalAmount" type="{http://www.fpml.org/2009/FpML-4-6}originalAmountType"/>
 *         &lt;element name="amount" type="{http://www.fpml.org/2009/FpML-4-6}amountType"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="evergreenOption" type="{http://www.fpml.org/2009/FpML-4-6}evergreenOptionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "letterOfCreditType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "lcSummary",
    "lcType",
    "lcPurpose",
    "borrowerPartyReference",
    "beneficiaryPartyReference",
    "originalAmount",
    "amount",
    "effectiveDate",
    "expiryDate",
    "evergreenOption"
})
public class LetterOfCreditType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LcSummaryType lcSummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String lcType;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String lcPurpose;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected BeneficiaryPartyReferenceType beneficiaryPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected OriginalAmountType originalAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected AmountType amount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String effectiveDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String expiryDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected EvergreenOptionType evergreenOption;

    /**
     * Gets the value of the lcSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LcSummaryType }
     *     
     */
    public LcSummaryType getLcSummary() {
        return lcSummary;
    }

    /**
     * Sets the value of the lcSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LcSummaryType }
     *     
     */
    public void setLcSummary(LcSummaryType value) {
        this.lcSummary = value;
    }

    /**
     * Gets the value of the lcType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLcType() {
        return lcType;
    }

    /**
     * Sets the value of the lcType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLcType(String value) {
        this.lcType = value;
    }

    /**
     * Gets the value of the lcPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLcPurpose() {
        return lcPurpose;
    }

    /**
     * Sets the value of the lcPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLcPurpose(String value) {
        this.lcPurpose = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the beneficiaryPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiaryPartyReferenceType }
     *     
     */
    public BeneficiaryPartyReferenceType getBeneficiaryPartyReference() {
        return beneficiaryPartyReference;
    }

    /**
     * Sets the value of the beneficiaryPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiaryPartyReferenceType }
     *     
     */
    public void setBeneficiaryPartyReference(BeneficiaryPartyReferenceType value) {
        this.beneficiaryPartyReference = value;
    }

    /**
     * Gets the value of the originalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalAmountType }
     *     
     */
    public OriginalAmountType getOriginalAmount() {
        return originalAmount;
    }

    /**
     * Sets the value of the originalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalAmountType }
     *     
     */
    public void setOriginalAmount(OriginalAmountType value) {
        this.originalAmount = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAmount(AmountType value) {
        this.amount = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the evergreenOption property.
     * 
     * @return
     *     possible object is
     *     {@link EvergreenOptionType }
     *     
     */
    public EvergreenOptionType getEvergreenOption() {
        return evergreenOption;
    }

    /**
     * Sets the value of the evergreenOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link EvergreenOptionType }
     *     
     */
    public void setEvergreenOption(EvergreenOptionType value) {
        this.evergreenOption = value;
    }

}
