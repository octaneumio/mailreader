
package xml.generated.loan.issuance.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for priorAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="priorAmountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="globalAmount" type="{http://www.fpml.org/2009/FpML-4-6}globalAmountType"/>
 *         &lt;element name="shareAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "priorAmountType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "globalAmount",
    "shareAmount"
})
public class PriorAmountType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected GlobalAmountType globalAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ShareAmountType shareAmount;

    /**
     * Gets the value of the globalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalAmountType }
     *     
     */
    public GlobalAmountType getGlobalAmount() {
        return globalAmount;
    }

    /**
     * Sets the value of the globalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalAmountType }
     *     
     */
    public void setGlobalAmount(GlobalAmountType value) {
        this.globalAmount = value;
    }

    /**
     * Gets the value of the shareAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareAmountType }
     *     
     */
    public ShareAmountType getShareAmount() {
        return shareAmount;
    }

    /**
     * Sets the value of the shareAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareAmountType }
     *     
     */
    public void setShareAmount(ShareAmountType value) {
        this.shareAmount = value;
    }

}
