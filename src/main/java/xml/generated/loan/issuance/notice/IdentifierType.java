
package xml.generated.loan.issuance.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for identifierType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="identifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="partyReference" type="{http://www.fpml.org/2009/FpML-4-6}partyReferenceType"/>
 *         &lt;element name="contractId" type="{http://www.fpml.org/2009/FpML-4-6}contractIdType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identifierType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "partyReference",
    "contractId"
})
public class IdentifierType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected PartyReferenceType partyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ContractIdType contractId;

    /**
     * Gets the value of the partyReference property.
     * 
     * @return
     *     possible object is
     *     {@link PartyReferenceType }
     *     
     */
    public PartyReferenceType getPartyReference() {
        return partyReference;
    }

    /**
     * Sets the value of the partyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyReferenceType }
     *     
     */
    public void setPartyReference(PartyReferenceType value) {
        this.partyReference = value;
    }

    /**
     * Gets the value of the contractId property.
     * 
     * @return
     *     possible object is
     *     {@link ContractIdType }
     *     
     */
    public ContractIdType getContractId() {
        return contractId;
    }

    /**
     * Sets the value of the contractId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContractIdType }
     *     
     */
    public void setContractId(ContractIdType value) {
        this.contractId = value;
    }

}
