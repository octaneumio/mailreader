
package xml.generated.loan.pricing.change.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for marginRateChangeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="marginRateChangeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="priorRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="postRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "marginRateChangeType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "effectiveDate",
    "priorRate",
    "postRate"
})
public class MarginRateChangeType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String effectiveDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String priorRate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String postRate;

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the priorRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorRate() {
        return priorRate;
    }

    /**
     * Sets the value of the priorRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorRate(String value) {
        this.priorRate = value;
    }

    /**
     * Gets the value of the postRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostRate() {
        return postRate;
    }

    /**
     * Sets the value of the postRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostRate(String value) {
        this.postRate = value;
    }

}
