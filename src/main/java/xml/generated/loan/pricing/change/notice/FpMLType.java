
package xml.generated.loan.pricing.change.notice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FpMLType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FpMLType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.fpml.org/2009/FpML-4-6}headerType"/>
 *         &lt;element name="noticeDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eventId" type="{http://www.fpml.org/2009/FpML-4-6}eventIdType"/>
 *         &lt;element name="agentBankPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}agentBankPartyReferenceType"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="dealSummary" type="{http://www.fpml.org/2009/FpML-4-6}dealSummaryType"/>
 *         &lt;element name="facilitySummary" type="{http://www.fpml.org/2009/FpML-4-6}facilitySummaryType"/>
 *         &lt;element name="exceptionFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pricingChangeReason" type="{http://www.fpml.org/2009/FpML-4-6}pricingChangeReasonType"/>
 *         &lt;element name="marginRateChange" type="{http://www.fpml.org/2009/FpML-4-6}marginRateChangeType"/>
 *         &lt;element name="party" type="{http://www.fpml.org/2009/FpML-4-6}partyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FpMLType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "header",
    "noticeDate",
    "eventId",
    "agentBankPartyReference",
    "borrowerPartyReference",
    "dealSummary",
    "facilitySummary",
    "exceptionFlag",
    "comments",
    "pricingChangeReason",
    "marginRateChange",
    "party"
})
@XmlRootElement(name="FpML", namespace = "http://www.fpml.org/2009/FpML-4-6")

public class FpMLType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected HeaderType header;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String noticeDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected EventIdType eventId;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected AgentBankPartyReferenceType agentBankPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected DealSummaryType dealSummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected FacilitySummaryType facilitySummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String exceptionFlag;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String comments;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected PricingChangeReasonType pricingChangeReason;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected MarginRateChangeType marginRateChange;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6")
    protected List<PartyType> party;
    @XmlAttribute(name = "version")
    protected String version;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the noticeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoticeDate() {
        return noticeDate;
    }

    /**
     * Sets the value of the noticeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoticeDate(String value) {
        this.noticeDate = value;
    }

    /**
     * Gets the value of the eventId property.
     * 
     * @return
     *     possible object is
     *     {@link EventIdType }
     *     
     */
    public EventIdType getEventId() {
        return eventId;
    }

    /**
     * Sets the value of the eventId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventIdType }
     *     
     */
    public void setEventId(EventIdType value) {
        this.eventId = value;
    }

    /**
     * Gets the value of the agentBankPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    public AgentBankPartyReferenceType getAgentBankPartyReference() {
        return agentBankPartyReference;
    }

    /**
     * Sets the value of the agentBankPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    public void setAgentBankPartyReference(AgentBankPartyReferenceType value) {
        this.agentBankPartyReference = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the dealSummary property.
     * 
     * @return
     *     possible object is
     *     {@link DealSummaryType }
     *     
     */
    public DealSummaryType getDealSummary() {
        return dealSummary;
    }

    /**
     * Sets the value of the dealSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealSummaryType }
     *     
     */
    public void setDealSummary(DealSummaryType value) {
        this.dealSummary = value;
    }

    /**
     * Gets the value of the facilitySummary property.
     * 
     * @return
     *     possible object is
     *     {@link FacilitySummaryType }
     *     
     */
    public FacilitySummaryType getFacilitySummary() {
        return facilitySummary;
    }

    /**
     * Sets the value of the facilitySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilitySummaryType }
     *     
     */
    public void setFacilitySummary(FacilitySummaryType value) {
        this.facilitySummary = value;
    }

    /**
     * Gets the value of the exceptionFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionFlag() {
        return exceptionFlag;
    }

    /**
     * Sets the value of the exceptionFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionFlag(String value) {
        this.exceptionFlag = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the pricingChangeReason property.
     * 
     * @return
     *     possible object is
     *     {@link PricingChangeReasonType }
     *     
     */
    public PricingChangeReasonType getPricingChangeReason() {
        return pricingChangeReason;
    }

    /**
     * Sets the value of the pricingChangeReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingChangeReasonType }
     *     
     */
    public void setPricingChangeReason(PricingChangeReasonType value) {
        this.pricingChangeReason = value;
    }

    /**
     * Gets the value of the marginRateChange property.
     * 
     * @return
     *     possible object is
     *     {@link MarginRateChangeType }
     *     
     */
    public MarginRateChangeType getMarginRateChange() {
        return marginRateChange;
    }

    /**
     * Sets the value of the marginRateChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarginRateChangeType }
     *     
     */
    public void setMarginRateChange(MarginRateChangeType value) {
        this.marginRateChange = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    public List<PartyType> getParty() {
        if (party == null) {
            party = new ArrayList<PartyType>();
        }
        return this.party;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
