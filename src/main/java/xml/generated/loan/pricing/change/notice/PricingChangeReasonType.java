
package xml.generated.loan.pricing.change.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for pricingChangeReasonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pricingChangeReasonType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="pricingChangeReasonScheme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pricingChangeReasonType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "value"
})
public class PricingChangeReasonType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "pricingChangeReasonScheme")
    protected String pricingChangeReasonScheme;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the pricingChangeReasonScheme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingChangeReasonScheme() {
        return pricingChangeReasonScheme;
    }

    /**
     * Sets the value of the pricingChangeReasonScheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingChangeReasonScheme(String value) {
        this.pricingChangeReasonScheme = value;
    }

}
