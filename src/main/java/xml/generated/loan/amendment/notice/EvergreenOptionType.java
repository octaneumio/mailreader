
package xml.generated.loan.amendment.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for evergreenOptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="evergreenOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extensionNoticePeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extensionPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="finalExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "evergreenOptionType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "extensionNoticePeriod",
    "extensionPeriod",
    "finalExpirationDate"
})
public class EvergreenOptionType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String extensionNoticePeriod;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String extensionPeriod;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String finalExpirationDate;

    /**
     * Gets the value of the extensionNoticePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensionNoticePeriod() {
        return extensionNoticePeriod;
    }

    /**
     * Sets the value of the extensionNoticePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensionNoticePeriod(String value) {
        this.extensionNoticePeriod = value;
    }

    /**
     * Gets the value of the extensionPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensionPeriod() {
        return extensionPeriod;
    }

    /**
     * Sets the value of the extensionPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensionPeriod(String value) {
        this.extensionPeriod = value;
    }

    /**
     * Gets the value of the finalExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinalExpirationDate() {
        return finalExpirationDate;
    }

    /**
     * Sets the value of the finalExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinalExpirationDate(String value) {
        this.finalExpirationDate = value;
    }

}
