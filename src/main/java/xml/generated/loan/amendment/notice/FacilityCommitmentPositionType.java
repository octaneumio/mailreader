
package xml.generated.loan.amendment.notice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for facilityCommitmentPositionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="facilityCommitmentPositionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentAmount" type="{http://www.fpml.org/2009/FpML-4-6}currentAmountType"/>
 *         &lt;element name="priorAmount" type="{http://www.fpml.org/2009/FpML-4-6}priorAmountType"/>
 *         &lt;element name="loanContractPosition" type="{http://www.fpml.org/2009/FpML-4-6}loanContractPositionType"/>
 *         &lt;element name="lcPosition" type="{http://www.fpml.org/2009/FpML-4-6}lcPositionType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facilityCommitmentPositionType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "currentAmount",
    "priorAmount",
    "loanContractPosition",
    "lcPosition"
})
public class FacilityCommitmentPositionType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected CurrentAmountType currentAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected PriorAmountType priorAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LoanContractPositionType loanContractPosition;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LcPositionType lcPosition;

    /**
     * Gets the value of the currentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentAmountType }
     *     
     */
    public CurrentAmountType getCurrentAmount() {
        return currentAmount;
    }

    /**
     * Sets the value of the currentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentAmountType }
     *     
     */
    public void setCurrentAmount(CurrentAmountType value) {
        this.currentAmount = value;
    }

    /**
     * Gets the value of the priorAmount property.
     * 
     * @return
     *     possible object is
     *     {@link PriorAmountType }
     *     
     */
    public PriorAmountType getPriorAmount() {
        return priorAmount;
    }

    /**
     * Sets the value of the priorAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorAmountType }
     *     
     */
    public void setPriorAmount(PriorAmountType value) {
        this.priorAmount = value;
    }

    /**
     * Gets the value of the loanContractPosition property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractPositionType }
     *     
     */
    public LoanContractPositionType getLoanContractPosition() {
        return loanContractPosition;
    }

    /**
     * Sets the value of the loanContractPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractPositionType }
     *     
     */
    public void setLoanContractPosition(LoanContractPositionType value) {
        this.loanContractPosition = value;
    }

    /**
     * Gets the value of the lcPosition property.
     * 
     * @return
     *     possible object is
     *     {@link LcPositionType }
     *     
     */
    public LcPositionType getLcPosition() {
        return lcPosition;
    }

    /**
     * Sets the value of the lcPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link LcPositionType }
     *     
     */
    public void setLcPosition(LcPositionType value) {
        this.lcPosition = value;
    }

}
