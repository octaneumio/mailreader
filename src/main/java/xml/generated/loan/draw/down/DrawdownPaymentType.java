
package xml.generated.loan.draw.down;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for drawdownPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="drawdownPaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shareLoanContractAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareLoanContractAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "drawdownPaymentType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "paymentDate",
    "shareLoanContractAmount"
})
public class DrawdownPaymentType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String paymentDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ShareLoanContractAmountType shareLoanContractAmount;

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDate(String value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the shareLoanContractAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareLoanContractAmountType }
     *     
     */
    public ShareLoanContractAmountType getShareLoanContractAmount() {
        return shareLoanContractAmount;
    }

    /**
     * Sets the value of the shareLoanContractAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareLoanContractAmountType }
     *     
     */
    public void setShareLoanContractAmount(ShareLoanContractAmountType value) {
        this.shareLoanContractAmount = value;
    }

}
