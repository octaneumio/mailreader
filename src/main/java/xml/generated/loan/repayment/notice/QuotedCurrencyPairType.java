
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for quotedCurrencyPairType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="quotedCurrencyPairType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currency1" type="{http://www.fpml.org/2009/FpML-4-6}currency1Type"/>
 *         &lt;element name="currency2" type="{http://www.fpml.org/2009/FpML-4-6}currency2Type"/>
 *         &lt;element name="quoteBasis" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "quotedCurrencyPairType", propOrder = {
    "currency1",
    "currency2",
    "quoteBasis"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class QuotedCurrencyPairType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Currency1Type currency1;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Currency2Type currency2;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String quoteBasis;

    /**
     * Gets the value of the currency1 property.
     * 
     * @return
     *     possible object is
     *     {@link Currency1Type }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public Currency1Type getCurrency1() {
        return currency1;
    }

    /**
     * Sets the value of the currency1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency1Type }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCurrency1(Currency1Type value) {
        this.currency1 = value;
    }

    /**
     * Gets the value of the currency2 property.
     * 
     * @return
     *     possible object is
     *     {@link Currency2Type }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public Currency2Type getCurrency2() {
        return currency2;
    }

    /**
     * Sets the value of the currency2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Currency2Type }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCurrency2(Currency2Type value) {
        this.currency2 = value;
    }

    /**
     * Gets the value of the quoteBasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getQuoteBasis() {
        return quoteBasis;
    }

    /**
     * Sets the value of the quoteBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setQuoteBasis(String value) {
        this.quoteBasis = value;
    }

}
