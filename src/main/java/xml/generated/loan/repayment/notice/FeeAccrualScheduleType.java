
package xml.generated.loan.repayment.notice;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for feeAccrualScheduleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="feeAccrualScheduleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lenderCommitmentPeriod" type="{http://www.fpml.org/2009/FpML-4-6}lenderCommitmentPeriodType"/>
 *         &lt;element name="feeRatePeriod" type="{http://www.fpml.org/2009/FpML-4-6}feeRatePeriodType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="feeAccrualPeriod" type="{http://www.fpml.org/2009/FpML-4-6}feeAccrualPeriodType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "feeAccrualScheduleType", propOrder = {
    "lenderCommitmentPeriod",
    "feeRatePeriod",
    "feeAccrualPeriod"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FeeAccrualScheduleType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LenderCommitmentPeriodType lenderCommitmentPeriod;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<FeeRatePeriodType> feeRatePeriod;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<FeeAccrualPeriodType> feeAccrualPeriod;

    /**
     * Gets the value of the lenderCommitmentPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link LenderCommitmentPeriodType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LenderCommitmentPeriodType getLenderCommitmentPeriod() {
        return lenderCommitmentPeriod;
    }

    /**
     * Sets the value of the lenderCommitmentPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link LenderCommitmentPeriodType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLenderCommitmentPeriod(LenderCommitmentPeriodType value) {
        this.lenderCommitmentPeriod = value;
    }

    /**
     * Gets the value of the feeRatePeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeRatePeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeRatePeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeRatePeriodType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<FeeRatePeriodType> getFeeRatePeriod() {
        if (feeRatePeriod == null) {
            feeRatePeriod = new ArrayList<FeeRatePeriodType>();
        }
        return this.feeRatePeriod;
    }

    /**
     * Gets the value of the feeAccrualPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feeAccrualPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeeAccrualPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeAccrualPeriodType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<FeeAccrualPeriodType> getFeeAccrualPeriod() {
        if (feeAccrualPeriod == null) {
            feeAccrualPeriod = new ArrayList<FeeAccrualPeriodType>();
        }
        return this.feeAccrualPeriod;
    }

}
