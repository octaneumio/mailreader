
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accrualAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accrualAmountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="globalAmount" type="{http://www.fpml.org/2009/FpML-4-6}globalAmountType"/>
 *         &lt;element name="shareAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accrualAmountType", propOrder = {
    "globalAmount",
    "shareAmount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class AccrualAmountType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected GlobalAmountType globalAmount;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected ShareAmountType shareAmount;

    /**
     * Gets the value of the globalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public GlobalAmountType getGlobalAmount() {
        return globalAmount;
    }

    /**
     * Sets the value of the globalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setGlobalAmount(GlobalAmountType value) {
        this.globalAmount = value;
    }

    /**
     * Gets the value of the shareAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public ShareAmountType getShareAmount() {
        return shareAmount;
    }

    /**
     * Sets the value of the shareAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setShareAmount(ShareAmountType value) {
        this.shareAmount = value;
    }

}
