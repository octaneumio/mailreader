
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for shareLoanContractAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="shareLoanContractAmountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currency" type="{http://www.fpml.org/2009/FpML-4-6}currencyType"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "shareLoanContractAmountType", propOrder = {
    "currency",
    "amount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class ShareLoanContractAmountType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CurrencyType currency;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String amount;

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public CurrencyType getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCurrency(CurrencyType value) {
        this.currency = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAmount(String value) {
        this.amount = value;
    }

}
