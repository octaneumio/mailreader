
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for drawdownPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="drawdownPaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shareLoanContractAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareLoanContractAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "drawdownPaymentType", propOrder = {
    "paymentDate",
    "shareLoanContractAmount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class DrawdownPaymentType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String paymentDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected ShareLoanContractAmountType shareLoanContractAmount;

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPaymentDate(String value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the shareLoanContractAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareLoanContractAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public ShareLoanContractAmountType getShareLoanContractAmount() {
        return shareLoanContractAmount;
    }

    /**
     * Sets the value of the shareLoanContractAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareLoanContractAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setShareLoanContractAmount(ShareLoanContractAmountType value) {
        this.shareLoanContractAmount = value;
    }

}
