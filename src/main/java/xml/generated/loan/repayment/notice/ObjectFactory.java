
package xml.generated.loan.repayment.notice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xml.generated.loan.repayment.notice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FpML_QNAME = new QName("http://www.fpml.org/2009/FpML-4-6", "FpML");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xml.generated.loan.repayment.notice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FpMLType }
     * 
     */
    public FpMLType createFpMLType() {
        return new FpMLType();
    }

    /**
     * Create an instance of {@link FacilitySummaryType }
     * 
     */
    public FacilitySummaryType createFacilitySummaryType() {
        return new FacilitySummaryType();
    }

    /**
     * Create an instance of {@link CurrencyType }
     * 
     */
    public CurrencyType createCurrencyType() {
        return new CurrencyType();
    }

    /**
     * Create an instance of {@link FeePaymentType }
     * 
     */
    public FeePaymentType createFeePaymentType() {
        return new FeePaymentType();
    }

    /**
     * Create an instance of {@link LenderPartyReferenceType }
     * 
     */
    public LenderPartyReferenceType createLenderPartyReferenceType() {
        return new LenderPartyReferenceType();
    }

    /**
     * Create an instance of {@link PartyIdType }
     * 
     */
    public PartyIdType createPartyIdType() {
        return new PartyIdType();
    }

    /**
     * Create an instance of {@link PartyType }
     * 
     */
    public PartyType createPartyType() {
        return new PartyType();
    }

    /**
     * Create an instance of {@link BorrowerPartyReferenceType }
     * 
     */
    public BorrowerPartyReferenceType createBorrowerPartyReferenceType() {
        return new BorrowerPartyReferenceType();
    }

    /**
     * Create an instance of {@link FeeAccrualPeriodType }
     * 
     */
    public FeeAccrualPeriodType createFeeAccrualPeriodType() {
        return new FeeAccrualPeriodType();
    }

    /**
     * Create an instance of {@link PositionAmountType }
     * 
     */
    public PositionAmountType createPositionAmountType() {
        return new PositionAmountType();
    }

    /**
     * Create an instance of {@link AgentBankPartyReferenceType }
     * 
     */
    public AgentBankPartyReferenceType createAgentBankPartyReferenceType() {
        return new AgentBankPartyReferenceType();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link DealSummaryType }
     * 
     */
    public DealSummaryType createDealSummaryType() {
        return new DealSummaryType();
    }

    /**
     * Create an instance of {@link LenderCommitmentPeriodType }
     * 
     */
    public LenderCommitmentPeriodType createLenderCommitmentPeriodType() {
        return new LenderCommitmentPeriodType();
    }

    /**
     * Create an instance of {@link SentByType }
     * 
     */
    public SentByType createSentByType() {
        return new SentByType();
    }

    /**
     * Create an instance of {@link FeeAccrualScheduleType }
     * 
     */
    public FeeAccrualScheduleType createFeeAccrualScheduleType() {
        return new FeeAccrualScheduleType();
    }

    /**
     * Create an instance of {@link MessageIdType }
     * 
     */
    public MessageIdType createMessageIdType() {
        return new MessageIdType();
    }

    /**
     * Create an instance of {@link InstrumentIdType }
     * 
     */
    public InstrumentIdType createInstrumentIdType() {
        return new InstrumentIdType();
    }

    /**
     * Create an instance of {@link FeeRatePeriodType }
     * 
     */
    public FeeRatePeriodType createFeeRatePeriodType() {
        return new FeeRatePeriodType();
    }

    /**
     * Create an instance of {@link SendToType }
     * 
     */
    public SendToType createSendToType() {
        return new SendToType();
    }

    /**
     * Create an instance of {@link AccrualAmountType }
     * 
     */
    public AccrualAmountType createAccrualAmountType() {
        return new AccrualAmountType();
    }

    /**
     * Create an instance of {@link GlobalAmountType }
     * 
     */
    public GlobalAmountType createGlobalAmountType() {
        return new GlobalAmountType();
    }

    /**
     * Create an instance of {@link HeaderType }
     * 
     */
    public HeaderType createHeaderType() {
        return new HeaderType();
    }

    /**
     * Create an instance of {@link FeeDayBasisType }
     * 
     */
    public FeeDayBasisType createFeeDayBasisType() {
        return new FeeDayBasisType();
    }

    /**
     * Create an instance of {@link ShareAmountType }
     * 
     */
    public ShareAmountType createShareAmountType() {
        return new ShareAmountType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FpMLType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.fpml.org/2009/FpML-4-6", name = "FpML")
    public JAXBElement<FpMLType> createFpML(FpMLType value) {
        return new JAXBElement<FpMLType>(_FpML_QNAME, FpMLType.class, null, value);
    }

}
