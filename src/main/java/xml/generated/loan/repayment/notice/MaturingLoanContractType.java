
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maturingLoanContractType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maturingLoanContractType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanContractSummary" type="{http://www.fpml.org/2009/FpML-4-6}loanContractSummaryType"/>
 *         &lt;element name="interestPayment" type="{http://www.fpml.org/2009/FpML-4-6}interestPaymentType"/>
 *         &lt;element name="interestAccrualSchedule" type="{http://www.fpml.org/2009/FpML-4-6}interestAccrualScheduleType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maturingLoanContractType", propOrder = {
    "loanContractSummary",
    "interestPayment",
    "interestAccrualSchedule"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class MaturingLoanContractType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LoanContractSummaryType loanContractSummary;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected InterestPaymentType interestPayment;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected InterestAccrualScheduleType interestAccrualSchedule;

    /**
     * Gets the value of the loanContractSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LoanContractSummaryType getLoanContractSummary() {
        return loanContractSummary;
    }

    /**
     * Sets the value of the loanContractSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLoanContractSummary(LoanContractSummaryType value) {
        this.loanContractSummary = value;
    }

    /**
     * Gets the value of the interestPayment property.
     * 
     * @return
     *     possible object is
     *     {@link InterestPaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public InterestPaymentType getInterestPayment() {
        return interestPayment;
    }

    /**
     * Sets the value of the interestPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestPaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setInterestPayment(InterestPaymentType value) {
        this.interestPayment = value;
    }

    /**
     * Gets the value of the interestAccrualSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link InterestAccrualScheduleType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public InterestAccrualScheduleType getInterestAccrualSchedule() {
        return interestAccrualSchedule;
    }

    /**
     * Sets the value of the interestAccrualSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestAccrualScheduleType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setInterestAccrualSchedule(InterestAccrualScheduleType value) {
        this.interestAccrualSchedule = value;
    }

}
