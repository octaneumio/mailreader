
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for letterOfCreditType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="letterOfCreditType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lcSummary" type="{http://www.fpml.org/2009/FpML-4-6}lcSummaryType"/>
 *         &lt;element name="lcType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lcPurpose" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="beneficiaryPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}beneficiaryPartyReferenceType"/>
 *         &lt;element name="originalAmount" type="{http://www.fpml.org/2009/FpML-4-6}originalAmountType"/>
 *         &lt;element name="amount" type="{http://www.fpml.org/2009/FpML-4-6}amountType"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "letterOfCreditType", propOrder = {
    "lcSummary",
    "lcType",
    "lcPurpose",
    "borrowerPartyReference",
    "beneficiaryPartyReference",
    "originalAmount",
    "amount",
    "effectiveDate",
    "expiryDate"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class LetterOfCreditType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LcSummaryType lcSummary;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String lcType;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String lcPurpose;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BeneficiaryPartyReferenceType beneficiaryPartyReference;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected OriginalAmountType originalAmount;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected AmountType amount;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String effectiveDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String expiryDate;

    /**
     * Gets the value of the lcSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LcSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LcSummaryType getLcSummary() {
        return lcSummary;
    }

    /**
     * Sets the value of the lcSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LcSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLcSummary(LcSummaryType value) {
        this.lcSummary = value;
    }

    /**
     * Gets the value of the lcType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getLcType() {
        return lcType;
    }

    /**
     * Sets the value of the lcType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLcType(String value) {
        this.lcType = value;
    }

    /**
     * Gets the value of the lcPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getLcPurpose() {
        return lcPurpose;
    }

    /**
     * Sets the value of the lcPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLcPurpose(String value) {
        this.lcPurpose = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the beneficiaryPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiaryPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public BeneficiaryPartyReferenceType getBeneficiaryPartyReference() {
        return beneficiaryPartyReference;
    }

    /**
     * Sets the value of the beneficiaryPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiaryPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setBeneficiaryPartyReference(BeneficiaryPartyReferenceType value) {
        this.beneficiaryPartyReference = value;
    }

    /**
     * Gets the value of the originalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public OriginalAmountType getOriginalAmount() {
        return originalAmount;
    }

    /**
     * Sets the value of the originalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setOriginalAmount(OriginalAmountType value) {
        this.originalAmount = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public AmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAmount(AmountType value) {
        this.amount = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:48+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

}
