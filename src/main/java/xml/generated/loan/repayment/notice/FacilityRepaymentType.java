
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for facilityRepaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="facilityRepaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="refusalAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adjustsCommitment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="repaymentDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.fpml.org/2009/FpML-4-6}amountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facilityRepaymentType", propOrder = {
    "refusalAllowed",
    "adjustsCommitment",
    "repaymentDate",
    "amount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FacilityRepaymentType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String refusalAllowed;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String adjustsCommitment;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String repaymentDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected AmountType amount;

    /**
     * Gets the value of the refusalAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getRefusalAllowed() {
        return refusalAllowed;
    }

    /**
     * Sets the value of the refusalAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setRefusalAllowed(String value) {
        this.refusalAllowed = value;
    }

    /**
     * Gets the value of the adjustsCommitment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getAdjustsCommitment() {
        return adjustsCommitment;
    }

    /**
     * Sets the value of the adjustsCommitment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAdjustsCommitment(String value) {
        this.adjustsCommitment = value;
    }

    /**
     * Gets the value of the repaymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getRepaymentDate() {
        return repaymentDate;
    }

    /**
     * Sets the value of the repaymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setRepaymentDate(String value) {
        this.repaymentDate = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public AmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAmount(AmountType value) {
        this.amount = value;
    }

}
