
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for interestRatePeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interestRatePeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rateFixingDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2018-12-14"/>
 *               &lt;enumeration value="2019-01-01"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="startDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2018-12-14"/>
 *               &lt;enumeration value="2019-01-01"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="endDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2018-12-31"/>
 *               &lt;enumeration value="2019-01-10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="floatingRateIndex" type="{http://www.fpml.org/2009/FpML-4-6}floatingRateIndexType"/>
 *         &lt;element name="indexTenor" type="{http://www.fpml.org/2009/FpML-4-6}indexTenorType"/>
 *         &lt;element name="interestRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="margin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="allInRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="interestDayBasis" type="{http://www.fpml.org/2009/FpML-4-6}interestDayBasisType"/>
 *         &lt;element name="tenor" type="{http://www.fpml.org/2009/FpML-4-6}tenorType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interestRatePeriodType", propOrder = {
    "rateFixingDate",
    "startDate",
    "endDate",
    "floatingRateIndex",
    "indexTenor",
    "interestRate",
    "margin",
    "allInRate",
    "interestDayBasis",
    "tenor"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class InterestRatePeriodType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String rateFixingDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String startDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String endDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FloatingRateIndexType floatingRateIndex;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected IndexTenorType indexTenor;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String interestRate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String margin;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String allInRate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected InterestDayBasisType interestDayBasis;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected TenorType tenor;

    /**
     * Gets the value of the rateFixingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getRateFixingDate() {
        return rateFixingDate;
    }

    /**
     * Sets the value of the rateFixingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setRateFixingDate(String value) {
        this.rateFixingDate = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the floatingRateIndex property.
     * 
     * @return
     *     possible object is
     *     {@link FloatingRateIndexType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FloatingRateIndexType getFloatingRateIndex() {
        return floatingRateIndex;
    }

    /**
     * Sets the value of the floatingRateIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatingRateIndexType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFloatingRateIndex(FloatingRateIndexType value) {
        this.floatingRateIndex = value;
    }

    /**
     * Gets the value of the indexTenor property.
     * 
     * @return
     *     possible object is
     *     {@link IndexTenorType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public IndexTenorType getIndexTenor() {
        return indexTenor;
    }

    /**
     * Sets the value of the indexTenor property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndexTenorType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setIndexTenor(IndexTenorType value) {
        this.indexTenor = value;
    }

    /**
     * Gets the value of the interestRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getInterestRate() {
        return interestRate;
    }

    /**
     * Sets the value of the interestRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setInterestRate(String value) {
        this.interestRate = value;
    }

    /**
     * Gets the value of the margin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getMargin() {
        return margin;
    }

    /**
     * Sets the value of the margin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMargin(String value) {
        this.margin = value;
    }

    /**
     * Gets the value of the allInRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getAllInRate() {
        return allInRate;
    }

    /**
     * Sets the value of the allInRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAllInRate(String value) {
        this.allInRate = value;
    }

    /**
     * Gets the value of the interestDayBasis property.
     * 
     * @return
     *     possible object is
     *     {@link InterestDayBasisType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public InterestDayBasisType getInterestDayBasis() {
        return interestDayBasis;
    }

    /**
     * Sets the value of the interestDayBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterestDayBasisType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setInterestDayBasis(InterestDayBasisType value) {
        this.interestDayBasis = value;
    }

    /**
     * Gets the value of the tenor property.
     * 
     * @return
     *     possible object is
     *     {@link TenorType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public TenorType getTenor() {
        return tenor;
    }

    /**
     * Sets the value of the tenor property.
     * 
     * @param value
     *     allowed object is
     *     {@link TenorType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setTenor(TenorType value) {
        this.tenor = value;
    }

}
