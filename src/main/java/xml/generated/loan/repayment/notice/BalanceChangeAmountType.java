
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for balanceChangeAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="balanceChangeAmountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shareAmount" type="{http://www.fpml.org/2009/FpML-4-6}shareAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "balanceChangeAmountType", propOrder = {
    "shareAmount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:34+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class BalanceChangeAmountType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:34+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected ShareAmountType shareAmount;

    /**
     * Gets the value of the shareAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ShareAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:34+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public ShareAmountType getShareAmount() {
        return shareAmount;
    }

    /**
     * Sets the value of the shareAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShareAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:30:34+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setShareAmount(ShareAmountType value) {
        this.shareAmount = value;
    }

}
