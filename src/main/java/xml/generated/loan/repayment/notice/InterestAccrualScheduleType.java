
package xml.generated.loan.repayment.notice;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for interestAccrualScheduleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="interestAccrualScheduleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interestRatePeriod" type="{http://www.fpml.org/2009/FpML-4-6}interestRatePeriodType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lenderLoanContractPeriod" type="{http://www.fpml.org/2009/FpML-4-6}lenderLoanContractPeriodType"/>
 *         &lt;element name="interestAccrualPeriod" type="{http://www.fpml.org/2009/FpML-4-6}interestAccrualPeriodType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "interestAccrualScheduleType", propOrder = {
    "interestRatePeriod",
    "lenderLoanContractPeriod",
    "interestAccrualPeriod"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class InterestAccrualScheduleType {

    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<InterestRatePeriodType> interestRatePeriod;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LenderLoanContractPeriodType lenderLoanContractPeriod;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<InterestAccrualPeriodType> interestAccrualPeriod;

    /**
     * Gets the value of the interestRatePeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interestRatePeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterestRatePeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterestRatePeriodType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<InterestRatePeriodType> getInterestRatePeriod() {
        if (interestRatePeriod == null) {
            interestRatePeriod = new ArrayList<InterestRatePeriodType>();
        }
        return this.interestRatePeriod;
    }

    /**
     * Gets the value of the lenderLoanContractPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link LenderLoanContractPeriodType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LenderLoanContractPeriodType getLenderLoanContractPeriod() {
        return lenderLoanContractPeriod;
    }

    /**
     * Sets the value of the lenderLoanContractPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link LenderLoanContractPeriodType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLenderLoanContractPeriod(LenderLoanContractPeriodType value) {
        this.lenderLoanContractPeriod = value;
    }

    /**
     * Gets the value of the interestAccrualPeriod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interestAccrualPeriod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterestAccrualPeriod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterestAccrualPeriodType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:01+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<InterestAccrualPeriodType> getInterestAccrualPeriod() {
        if (interestAccrualPeriod == null) {
            interestAccrualPeriod = new ArrayList<InterestAccrualPeriodType>();
        }
        return this.interestAccrualPeriod;
    }

}
