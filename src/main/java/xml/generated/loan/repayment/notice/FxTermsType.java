
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fxTermsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fxTermsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fxRate" type="{http://www.fpml.org/2009/FpML-4-6}fxRateType"/>
 *         &lt;element name="fixingDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fxTermsType", propOrder = {
    "fxRate",
    "fixingDate"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FxTermsType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FxRateType fxRate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String fixingDate;

    /**
     * Gets the value of the fxRate property.
     * 
     * @return
     *     possible object is
     *     {@link FxRateType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FxRateType getFxRate() {
        return fxRate;
    }

    /**
     * Sets the value of the fxRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link FxRateType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFxRate(FxRateType value) {
        this.fxRate = value;
    }

    /**
     * Gets the value of the fixingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getFixingDate() {
        return fixingDate;
    }

    /**
     * Sets the value of the fixingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFixingDate(String value) {
        this.fixingDate = value;
    }

}
