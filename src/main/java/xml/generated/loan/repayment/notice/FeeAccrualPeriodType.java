
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for feeAccrualPeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="feeAccrualPeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2018-10-01"/>
 *               &lt;enumeration value="2018-12-07"/>
 *               &lt;enumeration value="2018-12-19"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="endDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2018-12-06"/>
 *               &lt;enumeration value="2018-12-18"/>
 *               &lt;enumeration value="2018-12-31"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="accrualAmount" type="{http://www.fpml.org/2009/FpML-4-6}accrualAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "feeAccrualPeriodType", propOrder = {
    "startDate",
    "endDate",
    "accrualAmount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FeeAccrualPeriodType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String startDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String endDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected AccrualAmountType accrualAmount;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the accrualAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AccrualAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public AccrualAmountType getAccrualAmount() {
        return accrualAmount;
    }

    /**
     * Sets the value of the accrualAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAccrualAmount(AccrualAmountType value) {
        this.accrualAmount = value;
    }

}
