
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fxRateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fxRateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="quotedCurrencyPair" type="{http://www.fpml.org/2009/FpML-4-6}quotedCurrencyPairType"/>
 *         &lt;element name="rate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fxRateType", propOrder = {
    "quotedCurrencyPair",
    "rate"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FxRateType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected QuotedCurrencyPairType quotedCurrencyPair;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String rate;

    /**
     * Gets the value of the quotedCurrencyPair property.
     * 
     * @return
     *     possible object is
     *     {@link QuotedCurrencyPairType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public QuotedCurrencyPairType getQuotedCurrencyPair() {
        return quotedCurrencyPair;
    }

    /**
     * Sets the value of the quotedCurrencyPair property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuotedCurrencyPairType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setQuotedCurrencyPair(QuotedCurrencyPairType value) {
        this.quotedCurrencyPair = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setRate(String value) {
        this.rate = value;
    }

}
