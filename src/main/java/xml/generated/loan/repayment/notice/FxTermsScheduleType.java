
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fxTermsScheduleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fxTermsScheduleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fxTerms" type="{http://www.fpml.org/2009/FpML-4-6}fxTermsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fxTermsScheduleType", propOrder = {
    "startDate",
    "endDate",
    "fxTerms"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FxTermsScheduleType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String startDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String endDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FxTermsType fxTerms;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setStartDate(String value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the fxTerms property.
     * 
     * @return
     *     possible object is
     *     {@link FxTermsType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FxTermsType getFxTerms() {
        return fxTerms;
    }

    /**
     * Sets the value of the fxTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link FxTermsType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:28:52+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFxTerms(FxTermsType value) {
        this.fxTerms = value;
    }

}
