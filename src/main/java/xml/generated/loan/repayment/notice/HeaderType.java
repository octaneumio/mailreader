
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for headerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="headerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageId" type="{http://www.fpml.org/2009/FpML-4-6}messageIdType"/>
 *         &lt;element name="sentBy" type="{http://www.fpml.org/2009/FpML-4-6}sentByType"/>
 *         &lt;element name="sendTo" type="{http://www.fpml.org/2009/FpML-4-6}sendToType"/>
 *         &lt;element name="creationTimestamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "headerType", propOrder = {
    "messageId",
    "sentBy",
    "sendTo",
    "creationTimestamp"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class HeaderType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected MessageIdType messageId;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected SentByType sentBy;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected SendToType sendTo;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String creationTimestamp;

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link MessageIdType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public MessageIdType getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageIdType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMessageId(MessageIdType value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the sentBy property.
     * 
     * @return
     *     possible object is
     *     {@link SentByType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public SentByType getSentBy() {
        return sentBy;
    }

    /**
     * Sets the value of the sentBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link SentByType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setSentBy(SentByType value) {
        this.sentBy = value;
    }

    /**
     * Gets the value of the sendTo property.
     * 
     * @return
     *     possible object is
     *     {@link SendToType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public SendToType getSendTo() {
        return sendTo;
    }

    /**
     * Sets the value of the sendTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendToType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setSendTo(SendToType value) {
        this.sendTo = value;
    }

    /**
     * Gets the value of the creationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getCreationTimestamp() {
        return creationTimestamp;
    }

    /**
     * Sets the value of the creationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCreationTimestamp(String value) {
        this.creationTimestamp = value;
    }

}
