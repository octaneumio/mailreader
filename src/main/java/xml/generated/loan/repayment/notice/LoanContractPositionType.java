
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loanContractPositionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loanContractPositionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanContractSummary" type="{http://www.fpml.org/2009/FpML-4-6}loanContractSummaryType"/>
 *         &lt;element name="currentAmount" type="{http://www.fpml.org/2009/FpML-4-6}currentAmountType"/>
 *         &lt;element name="priorAmount" type="{http://www.fpml.org/2009/FpML-4-6}priorAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loanContractPositionType", propOrder = {
    "loanContractSummary",
    "currentAmount",
    "priorAmount"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class LoanContractPositionType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LoanContractSummaryType loanContractSummary;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected CurrentAmountType currentAmount;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected PriorAmountType priorAmount;

    /**
     * Gets the value of the loanContractSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LoanContractSummaryType getLoanContractSummary() {
        return loanContractSummary;
    }

    /**
     * Sets the value of the loanContractSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLoanContractSummary(LoanContractSummaryType value) {
        this.loanContractSummary = value;
    }

    /**
     * Gets the value of the currentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public CurrentAmountType getCurrentAmount() {
        return currentAmount;
    }

    /**
     * Sets the value of the currentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setCurrentAmount(CurrentAmountType value) {
        this.currentAmount = value;
    }

    /**
     * Gets the value of the priorAmount property.
     * 
     * @return
     *     possible object is
     *     {@link PriorAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public PriorAmountType getPriorAmount() {
        return priorAmount;
    }

    /**
     * Sets the value of the priorAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorAmountType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:31:06+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setPriorAmount(PriorAmountType value) {
        this.priorAmount = value;
    }

}
