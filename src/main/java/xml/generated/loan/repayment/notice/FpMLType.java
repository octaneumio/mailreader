
package xml.generated.loan.repayment.notice;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FpMLType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FpMLType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.fpml.org/2009/FpML-4-6}headerType"/>
 *         &lt;element name="noticeDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentBankPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}agentBankPartyReferenceType"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="lenderPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}lenderPartyReferenceType"/>
 *         &lt;element name="dealSummary" type="{http://www.fpml.org/2009/FpML-4-6}dealSummaryType"/>
 *         &lt;element name="facilitySummary" type="{http://www.fpml.org/2009/FpML-4-6}facilitySummaryType"/>
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="feePayment" type="{http://www.fpml.org/2009/FpML-4-6}feePaymentType"/>
 *         &lt;element name="feeAccrualSchedule" type="{http://www.fpml.org/2009/FpML-4-6}feeAccrualScheduleType"/>
 *         &lt;element name="party" type="{http://www.fpml.org/2009/FpML-4-6}partyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FpMLType", propOrder = {
    "header",
    "noticeDate",
    "agentBankPartyReference",
    "borrowerPartyReference",
    "lenderPartyReference",
    "dealSummary",
    "facilitySummary",
    "comments",
    "feePayment",
    "feeAccrualSchedule",
    "party"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class FpMLType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected HeaderType header;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String noticeDate;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected AgentBankPartyReferenceType agentBankPartyReference;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LenderPartyReferenceType lenderPartyReference;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected DealSummaryType dealSummary;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FacilitySummaryType facilitySummary;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String comments;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FeePaymentType feePayment;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FeeAccrualScheduleType feeAccrualSchedule;
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<PartyType> party;
    @XmlAttribute(name = "version")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String version;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the noticeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getNoticeDate() {
        return noticeDate;
    }

    /**
     * Sets the value of the noticeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setNoticeDate(String value) {
        this.noticeDate = value;
    }

    /**
     * Gets the value of the agentBankPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public AgentBankPartyReferenceType getAgentBankPartyReference() {
        return agentBankPartyReference;
    }

    /**
     * Sets the value of the agentBankPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentBankPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAgentBankPartyReference(AgentBankPartyReferenceType value) {
        this.agentBankPartyReference = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the lenderPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link LenderPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LenderPartyReferenceType getLenderPartyReference() {
        return lenderPartyReference;
    }

    /**
     * Sets the value of the lenderPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link LenderPartyReferenceType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLenderPartyReference(LenderPartyReferenceType value) {
        this.lenderPartyReference = value;
    }

    /**
     * Gets the value of the dealSummary property.
     * 
     * @return
     *     possible object is
     *     {@link DealSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public DealSummaryType getDealSummary() {
        return dealSummary;
    }

    /**
     * Sets the value of the dealSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DealSummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDealSummary(DealSummaryType value) {
        this.dealSummary = value;
    }

    /**
     * Gets the value of the facilitySummary property.
     * 
     * @return
     *     possible object is
     *     {@link FacilitySummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FacilitySummaryType getFacilitySummary() {
        return facilitySummary;
    }

    /**
     * Sets the value of the facilitySummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilitySummaryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFacilitySummary(FacilitySummaryType value) {
        this.facilitySummary = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the feePayment property.
     * 
     * @return
     *     possible object is
     *     {@link FeePaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FeePaymentType getFeePayment() {
        return feePayment;
    }

    /**
     * Sets the value of the feePayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeePaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFeePayment(FeePaymentType value) {
        this.feePayment = value;
    }

    /**
     * Gets the value of the feeAccrualSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link FeeAccrualScheduleType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FeeAccrualScheduleType getFeeAccrualSchedule() {
        return feeAccrualSchedule;
    }

    /**
     * Sets the value of the feeAccrualSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeAccrualScheduleType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFeeAccrualSchedule(FeeAccrualScheduleType value) {
        this.feeAccrualSchedule = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the party property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartyType }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<PartyType> getParty() {
        if (party == null) {
            party = new ArrayList<PartyType>();
        }
        return this.party;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:37+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setVersion(String value) {
        this.version = value;
    }

}
