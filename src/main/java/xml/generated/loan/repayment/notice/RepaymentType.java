
package xml.generated.loan.repayment.notice;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for repaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="repaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="facilityRepayment" type="{http://www.fpml.org/2009/FpML-4-6}facilityRepaymentType"/>
 *         &lt;element name="loanContractRepayment" type="{http://www.fpml.org/2009/FpML-4-6}loanContractRepaymentType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "repaymentType", propOrder = {
    "facilityRepayment",
    "loanContractRepayment"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
public class RepaymentType {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected FacilityRepaymentType facilityRepayment;
    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    protected LoanContractRepaymentType loanContractRepayment;

    /**
     * Gets the value of the facilityRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link FacilityRepaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public FacilityRepaymentType getFacilityRepayment() {
        return facilityRepayment;
    }

    /**
     * Sets the value of the facilityRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityRepaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setFacilityRepayment(FacilityRepaymentType value) {
        this.facilityRepayment = value;
    }

    /**
     * Gets the value of the loanContractRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractRepaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public LoanContractRepaymentType getLoanContractRepayment() {
        return loanContractRepayment;
    }

    /**
     * Sets the value of the loanContractRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractRepaymentType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2019-02-04T12:32:14+05:30", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setLoanContractRepayment(LoanContractRepaymentType value) {
        this.loanContractRepayment = value;
    }

}
