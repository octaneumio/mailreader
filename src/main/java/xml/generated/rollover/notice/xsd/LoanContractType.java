
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loanContractType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loanContractType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanContractSummary" type="{http://www.fpml.org/2009/FpML-4-6}loanContractSummaryType"/>
 *         &lt;element name="borrowerPartyReference" type="{http://www.fpml.org/2009/FpML-4-6}borrowerPartyReferenceType"/>
 *         &lt;element name="participationAmount" type="{http://www.fpml.org/2009/FpML-4-6}participationAmountType"/>
 *         &lt;element name="effectiveDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="currentInterestRatePeriod" type="{http://www.fpml.org/2009/FpML-4-6}currentInterestRatePeriodType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loanContractType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "loanContractSummary",
    "borrowerPartyReference",
    "participationAmount",
    "effectiveDate",
    "currentInterestRatePeriod"
})
public class LoanContractType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LoanContractSummaryType loanContractSummary;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected BorrowerPartyReferenceType borrowerPartyReference;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected ParticipationAmountType participationAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String effectiveDate;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected CurrentInterestRatePeriodType currentInterestRatePeriod;

    /**
     * Gets the value of the loanContractSummary property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractSummaryType }
     *     
     */
    public LoanContractSummaryType getLoanContractSummary() {
        return loanContractSummary;
    }

    /**
     * Sets the value of the loanContractSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractSummaryType }
     *     
     */
    public void setLoanContractSummary(LoanContractSummaryType value) {
        this.loanContractSummary = value;
    }

    /**
     * Gets the value of the borrowerPartyReference property.
     * 
     * @return
     *     possible object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public BorrowerPartyReferenceType getBorrowerPartyReference() {
        return borrowerPartyReference;
    }

    /**
     * Sets the value of the borrowerPartyReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowerPartyReferenceType }
     *     
     */
    public void setBorrowerPartyReference(BorrowerPartyReferenceType value) {
        this.borrowerPartyReference = value;
    }

    /**
     * Gets the value of the participationAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ParticipationAmountType }
     *     
     */
    public ParticipationAmountType getParticipationAmount() {
        return participationAmount;
    }

    /**
     * Sets the value of the participationAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipationAmountType }
     *     
     */
    public void setParticipationAmount(ParticipationAmountType value) {
        this.participationAmount = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the currentInterestRatePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentInterestRatePeriodType }
     *     
     */
    public CurrentInterestRatePeriodType getCurrentInterestRatePeriod() {
        return currentInterestRatePeriod;
    }

    /**
     * Sets the value of the currentInterestRatePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentInterestRatePeriodType }
     *     
     */
    public void setCurrentInterestRatePeriod(CurrentInterestRatePeriodType value) {
        this.currentInterestRatePeriod = value;
    }

}
