
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for newLoanContractsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="newLoanContractsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanContract" type="{http://www.fpml.org/2009/FpML-4-6}loanContractType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "newLoanContractsType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "loanContract"
})
public class NewLoanContractsType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected LoanContractType loanContract;

    /**
     * Gets the value of the loanContract property.
     * 
     * @return
     *     possible object is
     *     {@link LoanContractType }
     *     
     */
    public LoanContractType getLoanContract() {
        return loanContract;
    }

    /**
     * Sets the value of the loanContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanContractType }
     *     
     */
    public void setLoanContract(LoanContractType value) {
        this.loanContract = value;
    }

}
