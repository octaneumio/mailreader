
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for headerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="headerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageId" type="{http://www.fpml.org/2009/FpML-4-6}messageIdType"/>
 *         &lt;element name="sentBy" type="{http://www.fpml.org/2009/FpML-4-6}sentByType"/>
 *         &lt;element name="sendTo" type="{http://www.fpml.org/2009/FpML-4-6}sendToType"/>
 *         &lt;element name="creationTimestamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "headerType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "messageId",
    "sentBy",
    "sendTo",
    "creationTimestamp"
})
public class HeaderType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected MessageIdType messageId;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected SentByType sentBy;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected SendToType sendTo;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String creationTimestamp;

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link MessageIdType }
     *     
     */
    public MessageIdType getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageIdType }
     *     
     */
    public void setMessageId(MessageIdType value) {
        this.messageId = value;
    }

    /**
     * Gets the value of the sentBy property.
     * 
     * @return
     *     possible object is
     *     {@link SentByType }
     *     
     */
    public SentByType getSentBy() {
        return sentBy;
    }

    /**
     * Sets the value of the sentBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link SentByType }
     *     
     */
    public void setSentBy(SentByType value) {
        this.sentBy = value;
    }

    /**
     * Gets the value of the sendTo property.
     * 
     * @return
     *     possible object is
     *     {@link SendToType }
     *     
     */
    public SendToType getSendTo() {
        return sendTo;
    }

    /**
     * Sets the value of the sendTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendToType }
     *     
     */
    public void setSendTo(SendToType value) {
        this.sendTo = value;
    }

    /**
     * Gets the value of the creationTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationTimestamp() {
        return creationTimestamp;
    }

    /**
     * Sets the value of the creationTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationTimestamp(String value) {
        this.creationTimestamp = value;
    }

}
