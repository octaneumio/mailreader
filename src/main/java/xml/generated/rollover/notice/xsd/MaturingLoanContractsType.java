
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maturingLoanContractsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maturingLoanContractsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="maturingLoanContract" type="{http://www.fpml.org/2009/FpML-4-6}maturingLoanContractType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maturingLoanContractsType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "maturingLoanContract"
})
public class MaturingLoanContractsType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected MaturingLoanContractType maturingLoanContract;

    /**
     * Gets the value of the maturingLoanContract property.
     * 
     * @return
     *     possible object is
     *     {@link MaturingLoanContractType }
     *     
     */
    public MaturingLoanContractType getMaturingLoanContract() {
        return maturingLoanContract;
    }

    /**
     * Sets the value of the maturingLoanContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaturingLoanContractType }
     *     
     */
    public void setMaturingLoanContract(MaturingLoanContractType value) {
        this.maturingLoanContract = value;
    }

}
