
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loanContractSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loanContractSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identifier" type="{http://www.fpml.org/2009/FpML-4-6}identifierType"/>
 *         &lt;element name="participationAmount" type="{http://www.fpml.org/2009/FpML-4-6}participationAmountType" minOccurs="0"/>
 *         &lt;element name="maturityDate">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="2008-11-06"/>
 *               &lt;enumeration value="2008-11-13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loanContractSummaryType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "identifier",
    "participationAmount",
    "maturityDate"
})
public class LoanContractSummaryType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected IdentifierType identifier;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6")
    protected ParticipationAmountType participationAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String maturityDate;

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setIdentifier(IdentifierType value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the participationAmount property.
     * 
     * @return
     *     possible object is
     *     {@link ParticipationAmountType }
     *     
     */
    public ParticipationAmountType getParticipationAmount() {
        return participationAmount;
    }

    /**
     * Sets the value of the participationAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipationAmountType }
     *     
     */
    public void setParticipationAmount(ParticipationAmountType value) {
        this.participationAmount = value;
    }

    /**
     * Gets the value of the maturityDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaturityDate() {
        return maturityDate;
    }

    /**
     * Sets the value of the maturityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaturityDate(String value) {
        this.maturityDate = value;
    }

}
