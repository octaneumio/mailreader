
package xml.generated.rollover.notice.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for facilityCommitmentPositionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="facilityCommitmentPositionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentAmount" type="{http://www.fpml.org/2009/FpML-4-6}currentAmountType"/>
 *         &lt;element name="priorAmount" type="{http://www.fpml.org/2009/FpML-4-6}priorAmountType"/>
 *         &lt;element name="loanContractPosition" type="{http://www.fpml.org/2009/FpML-4-6}loanContractPositionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facilityCommitmentPositionType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "currentAmount",
    "priorAmount",
    "loanContractPosition"
})
public class FacilityCommitmentPositionType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected CurrentAmountType currentAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected PriorAmountType priorAmount;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6")
    protected List<LoanContractPositionType> loanContractPosition;

    /**
     * Gets the value of the currentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentAmountType }
     *     
     */
    public CurrentAmountType getCurrentAmount() {
        return currentAmount;
    }

    /**
     * Sets the value of the currentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentAmountType }
     *     
     */
    public void setCurrentAmount(CurrentAmountType value) {
        this.currentAmount = value;
    }

    /**
     * Gets the value of the priorAmount property.
     * 
     * @return
     *     possible object is
     *     {@link PriorAmountType }
     *     
     */
    public PriorAmountType getPriorAmount() {
        return priorAmount;
    }

    /**
     * Sets the value of the priorAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriorAmountType }
     *     
     */
    public void setPriorAmount(PriorAmountType value) {
        this.priorAmount = value;
    }

    /**
     * Gets the value of the loanContractPosition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loanContractPosition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoanContractPosition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoanContractPositionType }
     * 
     * 
     */
    public List<LoanContractPositionType> getLoanContractPosition() {
        if (loanContractPosition == null) {
            loanContractPosition = new ArrayList<LoanContractPositionType>();
        }
        return this.loanContractPosition;
    }

}
