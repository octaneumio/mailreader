
package xml.generated.rollover.notice.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for facilitySummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="facilitySummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="instrumentId" type="{http://www.fpml.org/2009/FpML-4-6}instrumentIdType"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originalCommitmentAmount" type="{http://www.fpml.org/2009/FpML-4-6}originalCommitmentAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facilitySummaryType", namespace = "http://www.fpml.org/2009/FpML-4-6", propOrder = {
    "instrumentId",
    "description",
    "originalCommitmentAmount"
})
public class FacilitySummaryType {

    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected InstrumentIdType instrumentId;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected String description;
    @XmlElement(namespace = "http://www.fpml.org/2009/FpML-4-6", required = true)
    protected OriginalCommitmentAmountType originalCommitmentAmount;

    /**
     * Gets the value of the instrumentId property.
     * 
     * @return
     *     possible object is
     *     {@link InstrumentIdType }
     *     
     */
    public InstrumentIdType getInstrumentId() {
        return instrumentId;
    }

    /**
     * Sets the value of the instrumentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link InstrumentIdType }
     *     
     */
    public void setInstrumentId(InstrumentIdType value) {
        this.instrumentId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the originalCommitmentAmount property.
     * 
     * @return
     *     possible object is
     *     {@link OriginalCommitmentAmountType }
     *     
     */
    public OriginalCommitmentAmountType getOriginalCommitmentAmount() {
        return originalCommitmentAmount;
    }

    /**
     * Sets the value of the originalCommitmentAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginalCommitmentAmountType }
     *     
     */
    public void setOriginalCommitmentAmount(OriginalCommitmentAmountType value) {
        this.originalCommitmentAmount = value;
    }

}
