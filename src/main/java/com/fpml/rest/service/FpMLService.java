package com.fpml.rest.service;


import com.fpml.rest.model.FpmlMessageDTO;

import java.io.IOException;
import java.util.Date;
import java.util.List;


public interface FpMLService {

	FpmlMessageDTO getMessages(int id) throws IOException;

	List<FpmlMessageDTO> getAllMessages(Date startDate, Date endDate, String status);
	

}
