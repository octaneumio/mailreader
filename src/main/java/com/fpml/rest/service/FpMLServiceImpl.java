package com.fpml.rest.service;

import com.fpml.rest.model.FpmlMessageDTO;
import com.mysql.jdbc.StringUtils;
import mail.DBService;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service("fpMLService")
@Transactional
public class FpMLServiceImpl implements FpMLService {



	@Override
	public FpmlMessageDTO getMessages(int msgId) throws IOException {
		DBService dbService = new DBService();
		FpmlMessageDTO fpmlMessageDTO = dbService.getMessage(msgId);

		JSONObject jsonObj = XML.toJSONObject(fpmlMessageDTO.getMessage());
		System.out.println(jsonObj.toString());
		fpmlMessageDTO.setMessage(jsonObj.toString());


		for(Iterator iterator = jsonObj.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			System.out.println(jsonObj.get(key));
			getValue(jsonObj.get(key),jsonObj);

		}
		return fpmlMessageDTO;
	}

	private void getValue(Object o, JSONObject jsonObj) {



	}

	private JSONObject findElementsChildren(JSONObject element, String id) {
		if(element.isJsonObject()) {
			JsonObject jsonObject = element.getAsJsonObject();
			if(id.equals(jsonObject.get("id").getAsString())) {
				return jsonObject.get("children");
			} else {
				return findElementsChildren(element.get("children").getAsJsonArray(), id);
			}
		} else if(element.isJsonArray()) {
			JsonArray jsonArray = element.getAsJsonArray();
			for (JsonElement childElement : jsonArray) {
				JsonElement result = findElementsChildren(childElement, id);
				if(result != null) {
					return result;
				}
			}
		}

		return null;
	}

	@Override
	public List<FpmlMessageDTO> getAllMessages(Date startDate, Date endDate, String status) {
		DBService dbService = new DBService();
		List<FpmlMessageDTO> fpmlMessageDTOList = new ArrayList<>();
		if(!StringUtils.isNullOrEmpty(status)) {
			dbService.getAllMessages(startDate,endDate,status);
		} else {
			dbService.getAllMessages(startDate,endDate);
		}
		return null;
	}
}
