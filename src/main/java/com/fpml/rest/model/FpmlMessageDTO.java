package com.fpml.rest.model;

import java.util.Date;

public class FpmlMessageDTO {

	private String message;
	private String recordId;
	private Date createdOn;

	private Double lnct_org_amt_amt;
	private String lnct_partc_glb_curr;
	private Double lnct_partc_glb_amt;
	private String lnct_partc_shr_curr;
	private Double lnct_partc_shr_amt;
	private Date lnct_mat_dt;
	private String int_pay_calc_mthd;
	private Date int_pay_pydt;
	private String int_pay_glb_curr;
	private Double int_pay_glb_amt;
	private String int_pay_shr_curr;
	private Double int_pay_shr_amt;
	private Date int_acc_rate_fix_dt;
	private Date int_acc_rtpd_start_dt;
	private Date int_acc_rtpd_end_dt;
	private String int_acc_flt_rate_idx;
	private Integer int_acc_idx_tnr_multp;
	private String int_acc_idx_tnr_prd;
	private Double int_acc_int_rate;
	private Double int_acc_margin;
	private Double int_acc_all_in_rate;
	private String int_acc_int_day_basis;
	private Integer int_acc_tnr_multp;
	private String int_acc_tnr_prd;
	private Date int_acc_lndr_ln_start_dt;
	private Date int_acc_lndr_ln_end_dt;
	private String int_acc_lndr_shr_curr;
	private Double int_acc_lndr_shr_amt;
	private Double int_acc_lndr_shr_lnct_curr;
	private Double int_acc_lndr_shr_lnct_amt;
	private Date int_acc_acpd_start_dt;
	private Date int_acc_acpd_end_dt;
	private Double int_acc_acpd_shr_acc_curr;
	private Double int_acc_acpd_shr_acc_amt;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Double getLnct_org_amt_amt() {
		return lnct_org_amt_amt;
	}

	public void setLnct_org_amt_amt(Double lnct_org_amt_amt) {
		this.lnct_org_amt_amt = lnct_org_amt_amt;
	}

	public String getLnct_partc_glb_curr() {
		return lnct_partc_glb_curr;
	}

	public void setLnct_partc_glb_curr(String lnct_partc_glb_curr) {
		this.lnct_partc_glb_curr = lnct_partc_glb_curr;
	}

	public Double getLnct_partc_glb_amt() {
		return lnct_partc_glb_amt;
	}

	public void setLnct_partc_glb_amt(Double lnct_partc_glb_amt) {
		this.lnct_partc_glb_amt = lnct_partc_glb_amt;
	}

	public String getLnct_partc_shr_curr() {
		return lnct_partc_shr_curr;
	}

	public void setLnct_partc_shr_curr(String lnct_partc_shr_curr) {
		this.lnct_partc_shr_curr = lnct_partc_shr_curr;
	}

	public Double getLnct_partc_shr_amt() {
		return lnct_partc_shr_amt;
	}

	public void setLnct_partc_shr_amt(Double lnct_partc_shr_amt) {
		this.lnct_partc_shr_amt = lnct_partc_shr_amt;
	}

	public Date getLnct_mat_dt() {
		return lnct_mat_dt;
	}

	public void setLnct_mat_dt(Date lnct_mat_dt) {
		this.lnct_mat_dt = lnct_mat_dt;
	}

	public String getInt_pay_calc_mthd() {
		return int_pay_calc_mthd;
	}

	public void setInt_pay_calc_mthd(String int_pay_calc_mthd) {
		this.int_pay_calc_mthd = int_pay_calc_mthd;
	}

	public Date getInt_pay_pydt() {
		return int_pay_pydt;
	}

	public void setInt_pay_pydt(Date int_pay_pydt) {
		this.int_pay_pydt = int_pay_pydt;
	}

	public String getInt_pay_glb_curr() {
		return int_pay_glb_curr;
	}

	public void setInt_pay_glb_curr(String int_pay_glb_curr) {
		this.int_pay_glb_curr = int_pay_glb_curr;
	}

	public Double getInt_pay_glb_amt() {
		return int_pay_glb_amt;
	}

	public void setInt_pay_glb_amt(Double int_pay_glb_amt) {
		this.int_pay_glb_amt = int_pay_glb_amt;
	}

	public String getInt_pay_shr_curr() {
		return int_pay_shr_curr;
	}

	public void setInt_pay_shr_curr(String int_pay_shr_curr) {
		this.int_pay_shr_curr = int_pay_shr_curr;
	}

	public Double getInt_pay_shr_amt() {
		return int_pay_shr_amt;
	}

	public void setInt_pay_shr_amt(Double int_pay_shr_amt) {
		this.int_pay_shr_amt = int_pay_shr_amt;
	}

	public Date getInt_acc_rate_fix_dt() {
		return int_acc_rate_fix_dt;
	}

	public void setInt_acc_rate_fix_dt(Date int_acc_rate_fix_dt) {
		this.int_acc_rate_fix_dt = int_acc_rate_fix_dt;
	}

	public Date getInt_acc_rtpd_start_dt() {
		return int_acc_rtpd_start_dt;
	}

	public void setInt_acc_rtpd_start_dt(Date int_acc_rtpd_start_dt) {
		this.int_acc_rtpd_start_dt = int_acc_rtpd_start_dt;
	}

	public Date getInt_acc_rtpd_end_dt() {
		return int_acc_rtpd_end_dt;
	}

	public void setInt_acc_rtpd_end_dt(Date int_acc_rtpd_end_dt) {
		this.int_acc_rtpd_end_dt = int_acc_rtpd_end_dt;
	}

	public String getInt_acc_flt_rate_idx() {
		return int_acc_flt_rate_idx;
	}

	public void setInt_acc_flt_rate_idx(String int_acc_flt_rate_idx) {
		this.int_acc_flt_rate_idx = int_acc_flt_rate_idx;
	}

	public Integer getInt_acc_idx_tnr_multp() {
		return int_acc_idx_tnr_multp;
	}

	public void setInt_acc_idx_tnr_multp(Integer int_acc_idx_tnr_multp) {
		this.int_acc_idx_tnr_multp = int_acc_idx_tnr_multp;
	}

	public String getInt_acc_idx_tnr_prd() {
		return int_acc_idx_tnr_prd;
	}

	public void setInt_acc_idx_tnr_prd(String int_acc_idx_tnr_prd) {
		this.int_acc_idx_tnr_prd = int_acc_idx_tnr_prd;
	}

	public Double getInt_acc_int_rate() {
		return int_acc_int_rate;
	}

	public void setInt_acc_int_rate(Double int_acc_int_rate) {
		this.int_acc_int_rate = int_acc_int_rate;
	}

	public Double getInt_acc_margin() {
		return int_acc_margin;
	}

	public void setInt_acc_margin(Double int_acc_margin) {
		this.int_acc_margin = int_acc_margin;
	}

	public Double getInt_acc_all_in_rate() {
		return int_acc_all_in_rate;
	}

	public void setInt_acc_all_in_rate(Double int_acc_all_in_rate) {
		this.int_acc_all_in_rate = int_acc_all_in_rate;
	}

	public String getInt_acc_int_day_basis() {
		return int_acc_int_day_basis;
	}

	public void setInt_acc_int_day_basis(String int_acc_int_day_basis) {
		this.int_acc_int_day_basis = int_acc_int_day_basis;
	}

	public Integer getInt_acc_tnr_multp() {
		return int_acc_tnr_multp;
	}

	public void setInt_acc_tnr_multp(Integer int_acc_tnr_multp) {
		this.int_acc_tnr_multp = int_acc_tnr_multp;
	}

	public String getInt_acc_tnr_prd() {
		return int_acc_tnr_prd;
	}

	public void setInt_acc_tnr_prd(String int_acc_tnr_prd) {
		this.int_acc_tnr_prd = int_acc_tnr_prd;
	}

	public Date getInt_acc_lndr_ln_start_dt() {
		return int_acc_lndr_ln_start_dt;
	}

	public void setInt_acc_lndr_ln_start_dt(Date int_acc_lndr_ln_start_dt) {
		this.int_acc_lndr_ln_start_dt = int_acc_lndr_ln_start_dt;
	}

	public Date getInt_acc_lndr_ln_end_dt() {
		return int_acc_lndr_ln_end_dt;
	}

	public void setInt_acc_lndr_ln_end_dt(Date int_acc_lndr_ln_end_dt) {
		this.int_acc_lndr_ln_end_dt = int_acc_lndr_ln_end_dt;
	}

	public String getInt_acc_lndr_shr_curr() {
		return int_acc_lndr_shr_curr;
	}

	public void setInt_acc_lndr_shr_curr(String int_acc_lndr_shr_curr) {
		this.int_acc_lndr_shr_curr = int_acc_lndr_shr_curr;
	}

	public Double getInt_acc_lndr_shr_amt() {
		return int_acc_lndr_shr_amt;
	}

	public void setInt_acc_lndr_shr_amt(Double int_acc_lndr_shr_amt) {
		this.int_acc_lndr_shr_amt = int_acc_lndr_shr_amt;
	}

	public Double getInt_acc_lndr_shr_lnct_curr() {
		return int_acc_lndr_shr_lnct_curr;
	}

	public void setInt_acc_lndr_shr_lnct_curr(Double int_acc_lndr_shr_lnct_curr) {
		this.int_acc_lndr_shr_lnct_curr = int_acc_lndr_shr_lnct_curr;
	}

	public Double getInt_acc_lndr_shr_lnct_amt() {
		return int_acc_lndr_shr_lnct_amt;
	}

	public void setInt_acc_lndr_shr_lnct_amt(Double int_acc_lndr_shr_lnct_amt) {
		this.int_acc_lndr_shr_lnct_amt = int_acc_lndr_shr_lnct_amt;
	}

	public Date getInt_acc_acpd_start_dt() {
		return int_acc_acpd_start_dt;
	}

	public void setInt_acc_acpd_start_dt(Date int_acc_acpd_start_dt) {
		this.int_acc_acpd_start_dt = int_acc_acpd_start_dt;
	}

	public Date getInt_acc_acpd_end_dt() {
		return int_acc_acpd_end_dt;
	}

	public void setInt_acc_acpd_end_dt(Date int_acc_acpd_end_dt) {
		this.int_acc_acpd_end_dt = int_acc_acpd_end_dt;
	}

	public Double getInt_acc_acpd_shr_acc_curr() {
		return int_acc_acpd_shr_acc_curr;
	}

	public void setInt_acc_acpd_shr_acc_curr(Double int_acc_acpd_shr_acc_curr) {
		this.int_acc_acpd_shr_acc_curr = int_acc_acpd_shr_acc_curr;
	}

	public Double getInt_acc_acpd_shr_acc_amt() {
		return int_acc_acpd_shr_acc_amt;
	}

	public void setInt_acc_acpd_shr_acc_amt(Double int_acc_acpd_shr_acc_amt) {
		this.int_acc_acpd_shr_acc_amt = int_acc_acpd_shr_acc_amt;
	}

}
