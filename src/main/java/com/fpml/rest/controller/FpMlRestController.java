package com.fpml.rest.controller;


import com.fpml.rest.model.FpmlMessageDTO;
import com.fpml.rest.service.FpMLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class FpMlRestController {

	@Autowired
	FpMLService fpMLService;


	@RequestMapping(value = "/getAllMesssages/{startDate}/{endDate}/{status}", method = RequestMethod.GET)
	public ResponseEntity<List<FpmlMessageDTO>> getAllMesssages(@PathVariable("startDate") Date startDate, @PathVariable("endDate") Date endDate,
																@PathVariable("status") String status) {
		List<FpmlMessageDTO> users = fpMLService.getAllMessages(startDate,endDate,status);
		if(users.isEmpty()){
			return new ResponseEntity<List<FpmlMessageDTO>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<FpmlMessageDTO>>(users, HttpStatus.OK);
	}

	@RequestMapping(value = "/getMessage/{msgId}", method = RequestMethod.GET)
	public ResponseEntity<FpmlMessageDTO> getMessage(@PathVariable("msgId") int msgId) throws IOException {
		return new ResponseEntity<FpmlMessageDTO>(fpMLService.getMessages(msgId), HttpStatus.OK);
	}


}
