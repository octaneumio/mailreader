## Service to connect and read Office mails. Parse the XML attachments and save to DB.

### Prerequisites:
1) Java 8
2) Maven 3
3) Tomcat 7 or higher

### Build the Project

mvn clean install

### Run Application

MailReaderMain.java is the Main class which can be run to do the above process.

### Code explanation

### Mail connector and reading messages

1. Connect Office using java mail by loading all properties from config.properties
2. Once connection is successful, read all unread messages from INBOX folder.
3. Check for multipart attachment ( only XML )in the messages.
4. Save the xml attachments to /attachments-read

### Model class generation steps

5. Check /src/main/java/main-xsd/xml/loan/loan-ex01-drawdown-notice.xml. Right click and check 
   for any option like (Generate XSD from xml) in your IDE.
6. Generated XSD for (loan-ex01-drawdown-notice.xml) is available in 
   src/main/java/main-xsd/xml/loan/loan-ex01-drawdown-notice.xsd
7. Right click the generated XSD (loan-ex01-drawdown-notice.xsd) and check for any options like 
   (Generate Java code from XMl schema)
8. This option will be available in all IDE's like eclipse, intellij ( I have useed intellij)
9. Generate model classes are available in /src/main/java/xml/generated
10. Both xmls and xsds are moved to xml.generated packages

### Unmarshalling steps

11. Lets consider for example, "loan-ex01-drawdown-notice.xml" is received in mail.
12. Parse the xml and check for the (xsi:type="DrawdownNotice")
13. With this xsi:type, we are finding the unmarshalling class.
14. After successful unmarshalling, we are printing the objects to console.

### Saving and moving the xml to processed folder

15. Once after parsing is done, we are saving the filecontent and file name to MYSQL DB
16. Finally we are moving the xml attachment from /attachments-read to /attachments-processed.
17. Also the messages are marked as read after successful parsing.


 
